module scc_fifo
#(
  parameter FIFO_AW     = 7,
  parameter FIFO_OFFSET = 13'h0000,
  parameter RAM_AW      = 19
)
(
  // Clock and reset
  input   logic                 areset,
  input   logic                 clk,

  // Memory interfaces, 0 - is write side, 1 - is read side
  output  logic [ 1: 0]         ram_ena,
  input   logic [ 1: 0]         ram_busy,
  output  logic [ 1: 0][18: 0]  ram_a,
  output  logic [ 1: 0][ 3: 0]  ram_be,
  output  logic [ 1: 0][31: 0]  ram_d,
  output  logic [ 1: 0]         ram_we,
  input   logic [ 1: 0][31: 0]  ram_q,

  // FIFO interface
  input   logic                 ws_ena,
  output  logic                 ws_busy,
  input   logic        [31: 0]  ws_d,

  input   logic                 rs_ena,
  output  logic                 rs_busy,
  output  logic        [31: 0]  rs_q,
  
  output  logic   [FIFO_AW: 0]  rs_words
);

  localparam AW = FIFO_AW;
  logic [AW: 0] ws_ptr;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      ws_ptr <= '0;
    else
      if (ws_ena)
        ws_ptr <= ws_ptr + !ws_busy;

  always_comb begin
    ram_ena[0]  = ws_ena;
    ws_busy     = ram_busy[0];
    ram_a[0]    = { FIFO_OFFSET[RAM_AW - FIFO_AW - 1: 0], ws_ptr[AW-1: 0] };
    ram_be[0]   = '1;
    ram_d[0]    = ws_d;
    ram_we[0]   = '1;
  end
  
  logic [AW: 0] rs_ptr;

  always_ff@(posedge areset or posedge clk)
    if (areset)
      rs_ptr <= '0;
    else
      if (rs_ena)
        rs_ptr <= rs_ptr + !rs_busy;
  
  always_comb begin
    ram_ena[1]  = rs_ena;
    rs_busy     = ram_busy[1];
    ram_a[1]    = { FIFO_OFFSET[RAM_AW - FIFO_AW - 1: 0], rs_ptr[AW-1: 0] };
    ram_be[1]   = '1;
    ram_d[1]    = 'x;
    ram_we[1]   = '0;
    rs_q        = ram_q[1];
    
    rs_words    = ws_ptr - rs_ptr;
  end
  
endmodule
