set ccpipe_regs [get_registers -nowarn {*ccpipe*r_pipe[0]*}]
if {[get_collection_size $ccpipe_regs] != 0} {
  set_false_path -to $ccpipe_regs
}
