module ccpipe
#(
  parameter WIDTH  = 1,
  parameter STAGES = 3,
  parameter RVALUE = '0
)
(
  input  wire             areset,
  input  wire             clk,

  input  wire [WIDTH-1:0] d,
  output wire [WIDTH-1:0] q
);
  (* altera_attribute = "-name SYNCHRONIZER_IDENTIFICATION \"FORCED IF ASYNCHRONOUS\"" *)
  reg [STAGES-1:0][WIDTH-1:0] r_pipe;

  always@(posedge clk or posedge areset)
    if (areset) begin
      r_pipe <= RVALUE;
    end
    else begin
      integer i;
      r_pipe[0] <= d;
      for (i = 1; i < STAGES; i = i + 1)
        r_pipe[i] <= r_pipe[i-1];
    end

  assign q = r_pipe[STAGES-1];
    
endmodule
