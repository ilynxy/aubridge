module cchystreg
#(
  parameter HYST_CLKS     = 7,
  parameter CC_STAGES     = 3,
  parameter RESET_VALUE   = '0,
  parameter EDGES_REG   = "NO"
)
(
  input  logic    areset,
  input  logic    clk,
  
  input  logic    d,
  
  output logic    q,
  output logic    q_posedge,
  output logic    q_negedge
);

  logic cc_d;
  ccpipe#(
    .WIDTH(1), 
    .STAGES(CC_STAGES), 
    .RVALUE({ CC_STAGES { !(!RESET_VALUE) } })
    )
    ccpipe_d
    (
      .areset(areset),
      .clk(clk),
      
      .d(d),
      .q(cc_d)
    );

  hystreg#(
    .HYSTCLK(HYST_CLKS),
    .EDGES_REG(EDGES_REG)
    )
    hyst_d
    (
      .areset(areset),
      .clk(clk),
      .ena(1'b1),
      
      .d(cc_d),
      .q(q),
      
      .rise(q_posedge),
      .fall(q_negedge)
    );

endmodule
