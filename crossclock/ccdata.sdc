set ccdata_regs [get_registers -nowarn {*ccdata*r_d_clk0*}]
if {[get_collection_size $ccdata_regs] != 0} {
  set_false_path -from $ccdata_regs
}
