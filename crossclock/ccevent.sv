module ccevent
#(
  parameter STAGES = 2
)
(
  input  logic areset0,
  input  logic clk0,
  input  logic event0,
  output logic busy0,
  
  input  logic areset1,
  input  logic clk1,
  output logic event1
);
  
  generate
    if      (STAGES == 0) begin
      ccevent_0s
        ccevent_i
        (
          .areset0  (areset0),
          .clk0     (clk0),
          .event0   (event0),
          .busy0    (busy0),
          
          .areset1  (areset1),
          .clk1     (clk1),
          .event1   (event1)
        );
    end
    else if (STAGES == 1) begin
      ccevent_1s
        ccevent_i
        (
          .areset0  (areset0),
          .clk0     (clk0),
          .event0   (event0),
          .busy0    (busy0),
          
          .areset1  (areset1),
          .clk1     (clk1),
          .event1   (event1)
        );
    
    end
    else begin
      ccevent_ns#
      (
        .STAGES     (STAGES)
      )
        ccevent_i
        (
          .areset0  (areset0),
          .clk0     (clk0),
          .event0   (event0),
          .busy0    (busy0),
          
          .areset1  (areset1),
          .clk1     (clk1),
          .event1   (event1)
        );
    end
  endgenerate

endmodule

// clocks are the same, no need to synchronization
module ccevent_0s
(
  input  logic areset0,
  input  logic clk0,
  input  logic event0,
  output logic busy0,
  
  input  logic areset1,
  input  logic clk1,
  output logic event1
);
  
  assign event1 = event0;
  assign busy0  = '0;

endmodule

// clocks are synchronized
module ccevent_1s
(
  input  logic areset0,
  input  logic clk0,
  input  logic event0,
  output logic busy0,
  
  input  logic areset1,
  input  logic clk1,
  output logic event1
);

  logic r_level_clk0 = '0;
  logic r_level_clk1 = '0;

  always_ff@(posedge areset0 or posedge clk0)
    if (areset0)
      r_level_clk0 <= '0;
    else
      if (event0 && !busy0)
        r_level_clk0 <= !r_level_clk0;

  always_ff@(posedge areset1 or posedge clk1)
    if (areset1)
      r_level_clk1 <= '0;
    else
      r_level_clk1 <= r_level_clk0;

  assign busy0  = r_level_clk1 ^ r_level_clk0;
  assign event1 = r_level_clk0 ^ r_level_clk1;

endmodule

// clocks are unrelated
module ccevent_ns
#(
  parameter STAGES = 2
)
(
  input  logic areset0,
  input  logic clk0,
  input  logic event0,
  output logic busy0,
  
  input  logic areset1,
  input  logic clk1,
  output logic event1
);

  logic r_level_clk0 = '0;
  logic cc_level_clk0;
  
  logic r_level_clk1 = '0;
  wire  cc_level_clk1;

  assign busy0 = cc_level_clk1 ^ r_level_clk0;

  always_ff@(posedge areset0 or posedge clk0)
    if (areset0)
      r_level_clk0 <= '0;
    else
      if (event0 && !busy0)
        r_level_clk0 <= !r_level_clk0;

  always_ff@(posedge areset1 or posedge clk1)
    if (areset1)
      r_level_clk1 <= '0;
    else
      r_level_clk1 <= cc_level_clk0;

  assign event1 = cc_level_clk0 ^ r_level_clk1;
    
  ccpipe#(.WIDTH(1), .STAGES(STAGES))
    ccpipe_level_from_clk1
    (
      .areset   (areset0),
      .clk      (clk0),
      .d        (r_level_clk1),
      .q        (cc_level_clk1)
    );

  ccpipe#(.WIDTH(1), .STAGES(STAGES))
    ccpipe_level_from_clk0
    (
      .areset   (areset1),
      .clk      (clk1),
      .d        (r_level_clk0),
      .q        (cc_level_clk0)
    );

endmodule
