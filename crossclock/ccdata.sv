module ccdata
#(
  parameter WIDTH  = 1,
  parameter STAGES = 1,
  parameter RVALUE_CLK0  = '0,
  parameter RVALUE_CLK1  = '0
)
(
  input  logic              areset0,
  input  logic              clk0,
  input  logic [WIDTH-1:0]  d,
  
  input  logic              areset1,
  input  logic              clk1,
  output logic [WIDTH-1:0]  q
);

  logic busy0;
  logic event1;

  ccevent#(
    .STAGES(STAGES)
    )
    continous_event
    (
      .areset0(areset0),
      .clk0(clk0),
      .event0(!busy0),
      .busy0(busy0),
      
      .areset1(areset1),
      .clk1(clk1),
      .event1(event1)
    );

  logic [WIDTH-1:0] r_d_clk0;
  logic [WIDTH-1:0] r_q_clk1;

  generate
    if (STAGES == 0) begin // same clock

      always_comb
        r_d_clk0 = d;

      always_comb
        r_q_clk1 = r_d_clk0;

    end
    else begin

      always@(posedge areset1 or posedge clk1)
        if (areset1)
          r_q_clk1 <= RVALUE_CLK1;
        else
          if (event1)
            r_q_clk1 <= r_d_clk0;

      always@(posedge areset0 or posedge clk0)
        if (areset0)
          r_d_clk0 <= RVALUE_CLK0;
        else
          if (!busy0)
            r_d_clk0 <= d;

    end
  endgenerate

  assign q = r_q_clk1;

endmodule
