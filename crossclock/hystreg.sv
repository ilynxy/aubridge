module hystreg
#(
  parameter HYSTCLK     = 10,
  parameter RESETLEVEL  = 0,
  parameter EDGES_REG   = "NO"
)
(
  input  wire     areset,
  input  wire     clk,
  input  wire     ena,

  input  wire     d,
  output wire     q,
  
  output wire     rise,
  output wire     fall
);

  localparam _CW = $clog2(HYSTCLK);

  generate
    if (RESETLEVEL == 0) begin
      localparam _RESCNT  = '0;
      localparam _RESQ    = '0;
    end
    else begin
      localparam _RESCNT  = HYSTCLK - 1;
      localparam _RESQ    = '1;  
    end
  endgenerate
  
  typedef logic [_CW-1:0] cnt_t;
  cnt_t   r_cnt = cnt_t'(_RESCNT);
  logic   r_q   = logic'(_RESQ);

  wire n_max = (r_cnt == (HYSTCLK - 1));
  wire n_min = (r_cnt == 0);

  wire n_rise = (r_q == 0) && n_max;
  wire n_fall = (r_q == 1) && n_min;
  
  wire n_increase = (d == 1) && !n_max;
  wire n_decrease = (d == 0) && !n_min;

  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      r_cnt <= cnt_t'(_RESCNT);
      r_q   <= logic'(_RESQ);
    end
    else begin
      if (ena) begin
        if (n_increase || n_decrease)
          r_cnt <= r_cnt + cnt_t'(n_increase ? 1 : -1);
        
        if (n_rise || n_fall)
          r_q <= n_rise;  
      end
    end
  logic r_posedge;
  logic r_negedge;
  generate
    if (EDGES_REG == "YES") begin
      always_ff@(posedge areset or posedge clk)
        if (areset)
          { r_posedge, r_negedge } <= '0;
        else
          if (ena)
            { r_posedge, r_negedge } <= { n_rise, n_fall };
    end
    else begin
      always_comb
        { r_posedge, r_negedge } = { n_rise, n_fall };
    end
  endgenerate

  assign q    = r_q;
  assign rise = r_posedge;
  assign fall = r_negedge;

endmodule
