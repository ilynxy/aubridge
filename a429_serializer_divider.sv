module a429_serializer_divider
#(
  parameter N             =          4,
  parameter CLKFREQ       = 24_000_000
)
(
  input   logic                 areset,
  input   logic                 clk,
  
  output  logic                 ena_1ms,
  
  output  logic                 ena_mes1t,
  output  logic                 ena_mes4t,
  
  input   logic [N-1: 0][ 1: 0] ssel,
  output  logic [N-1: 0]        ena_tx2t
);

  function integer cdivr(input integer a, input integer b);
    cdivr = (a + (b / 2)) / b;
  endfunction

  localparam A429_MAX_FREQ = 100_000;
  localparam A429_MIN_FREQ =  12_500;
  localparam A429_MES_FREQ = A429_MAX_FREQ * 8;

  localparam DIVC   = cdivr(CLKFREQ, A429_MES_FREQ);
  localparam DIVCW  = $clog2(DIVC);

  
  typedef logic [DIVCW-1: 0] prediv_t;
  
  prediv_t prediv;

  logic prediv_wrap;
  always_comb
    prediv_wrap = (prediv == (DIVC - 1));

  always_ff@(posedge areset or posedge clk)
    if (areset)
      prediv <= '0;
    else
      if (prediv_wrap)
        prediv <= '0;
      else
        prediv <= prediv + 1'b1;

  localparam DIVA  = cdivr(A429_MES_FREQ, A429_MIN_FREQ * 2);
  localparam DIVAW = $clog2(DIVA);
  
  typedef logic [DIVAW-1: 0] maskcounter_t;
  
  maskcounter_t mcnt;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      mcnt <= '0;
    else
      if (prediv_wrap)
        mcnt <= mcnt + 1'b1;

  logic [N-1: 0] r_ena_tx2t;
  
  maskcounter_t [ 3: 0] masks;
  always_comb begin
    masks[0] = maskcounter_t'( 4 - 1);
    masks[1] = maskcounter_t'( 8 - 1);
    masks[2] = maskcounter_t'(16 - 1);
    masks[3] = maskcounter_t'(32 - 1);
  end
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      r_ena_tx2t    <= '0;
    else
      for (int i = 0; i < N; ++i)
        r_ena_tx2t[i] <= ( (mcnt & masks[ssel[i]]) == '0) && prediv_wrap;
    
  logic r_ena_mes1t;
  logic r_ena_mes4t;

  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      r_ena_mes1t <= '0;
      r_ena_mes4t <= '0;
    end
    else begin
      r_ena_mes1t <= ( (mcnt & maskcounter_t'( 1 - 1)) == '0) && prediv_wrap;
      r_ena_mes4t <= ( (mcnt & maskcounter_t'( 4 - 1)) == '0) && prediv_wrap;
    end

  always_comb begin
    ena_tx2t  = r_ena_tx2t;
    ena_mes1t = r_ena_mes1t;
    ena_mes4t = r_ena_mes4t;    
  end
  
  //
  logic [15: 0] div1ms = '0;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      div1ms <= '0;
    else
      if (div1ms == (CLKFREQ / 1000 - 1))
        div1ms <= '0;
      else
        div1ms <= div1ms + 1'b1;

  logic r_ena_1ms = '0;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      r_ena_1ms <= '0;
    else
      r_ena_1ms <= (div1ms == (CLKFREQ / 1000 - 1));
  
  always_comb
    ena_1ms = r_ena_1ms;

endmodule

