module ram_manager
#(
  parameter N = 4
)
(
  input   logic                 areset,
  input   logic                 clk,
  input   logic                 clkx2,
  
  // Arbitrator interface
  input   logic [N-1: 0]        ena,
  output  logic [N-1: 0]        busy,
  input   logic [N-1: 0][18: 0] a,
  input   logic [N-1: 0][ 3: 0] be,
  input   logic [N-1: 0][31: 0] d,
  input   logic [N-1: 0]        we,
  output  logic [N-1: 0][31: 0] q,
  output  logic [N-1: 0]        qv,

  // ASRAM interface
  output  wire          [ 2: 0] ASRAM_CE_N,
  inout   wire          [31: 0] ASRAM_D,
  output  wire          [18: 0] ASRAM_A,
  output  wire          [ 3: 0] ASRAM_BE_N,
  output  wire                  ASRAM_WE_N,
  output  wire                  ASRAM_OE_N
);

  logic         ram_ena;
  logic [18: 0] ram_a;
  logic [ 3: 0] ram_be;
  logic [31: 0] ram_d;
  logic [31: 0] ram_q;
  logic         ram_qv;
  logic         ram_we;

  asram_CY7C1062_adapter
    asram_adapter
    (
      .areset       (areset),
      .clk          (clk),
      .clkx2        (clkx2),

      .ena          (ram_ena),
      .a            (ram_a),
      .be           (ram_be),
      .d            (ram_d),
      .q            (ram_q),
      .qv           (ram_qv),
      .we           (ram_we),

      .ASRAM_CE_N   (ASRAM_CE_N),
      .ASRAM_D      (ASRAM_D),
      .ASRAM_A      (ASRAM_A),
      .ASRAM_BE_N   (ASRAM_BE_N),
      .ASRAM_WE_N   (ASRAM_WE_N),
      .ASRAM_OE_N   (ASRAM_OE_N)
    );
  
  ram_arbitrator#( .N ( N ) )
    arbitrator
    (
      .areset       (areset),
      .clk          (clk),

      .ena          (ena),
      .busy         (busy),
      .a            (a),
      .be           (be),
      .d            (d),
      .we           (we),
      .q            (q),
      .qv           (qv),

      .ram_ena      (ram_ena),
      .ram_a        (ram_a),
      .ram_be       (ram_be),
      .ram_d        (ram_d),
      .ram_we       (ram_we),
      .ram_q        (ram_q),
      .ram_qv       (ram_qv)
    );

endmodule
