module a429_serializer
(
  input   logic         areset,
  input   logic         clk,
  
  input   logic         ena,
  
  input   logic [31: 0] d,
  
  input   logic         load,
  output  logic         busy,

  output  logic         tx_a,
  output  logic         tx_b
);

/*
  logic [ 6: 0] bc;
  logic         r_busy;
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      bc <= '1;
      r_busy <= '0;
    end
    else
      if (ena) begin
        if (load) begin
          bc     <= '0;
          r_busy <= '1;
        end
        else begin
          r_busy <= !(bc[6] && bc[2] && bc[1]);
          bc     <= bc + busy;
        end
      end
*/

  logic d_load;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      d_load <= '0;
    else
      d_load <= (d_load || load) && !ena;

  logic [ 6: 0] bc;
  logic         r_busy;
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      bc <= '1;
      r_busy <= '0;
    end
    else
      if (ena) begin
        if (load || d_load) begin
          bc     <= '0;
          r_busy <= '1;
        end
        else begin
          r_busy <= !(bc[6] && bc[2] && bc[1]);
          bc     <= bc + busy;
        end
      end

  always_comb
    busy = r_busy || d_load;

/*    
  logic [31: 0] r_sh;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      r_sh      <= '0;
    else
      if (ena) begin
        if (load)
          r_sh <= d;
        else
          if (bc[0])
            r_sh <= { r_sh[30: 0], 1'bx };
      end
*/

  function logic [23: 0] rbo24(input logic [23: 0] x);
    for (int i = 0; i < 24; i ++)
      rbo24[i] = x[24 - i - 1];
  endfunction

  function logic [ 7: 0] rbo8(input logic [ 7: 0] x);
    for (int i = 0; i < 8; i ++)
      rbo8[i] = x[8 - i - 1];  
  endfunction

  logic [31: 0] r_sh;
  always_ff@(posedge areset or posedge clk) begin : shiftreg
    if (areset)
      r_sh      <= '0;
    else begin
      if (load) begin
`ifdef REVERSE_A429_ADDRESS
        r_sh <= { d[ 7: 0], rbo24(d[31: 8]) }; // reverse bit order in data part
`else
        r_sh <= { rbo8(d[ 7: 0]), rbo24(d[31: 8]) }; // reverse bit order in data part
`endif
      end
      else
        if (ena && !bc[6] && bc[0])
          r_sh <= { r_sh[30: 0], 1'bx };
    end
  end : shiftreg

  logic idle;
  logic value;
  
  always_comb begin
    idle  = bc[6] || bc[0];
    value = r_sh[31];
  end
  
  logic r_a;
  logic r_b;
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      r_a <= '0;
      r_b <= '0;
    end
    else begin
      if (ena) begin
        r_a <= idle ? 1'b0 :  value;
        r_b <= idle ? 1'b0 : ~value;
      end
    end

  always_comb begin
    tx_a = r_a;
    tx_b = r_b;
  end

endmodule
