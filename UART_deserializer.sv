module UART_deserializer
#(
  parameter CLKFREQ = 24_000_000,
  parameter BITRATE =  2_000_000
)
(
  input   logic         areset,
  input   logic         clk,
  
  input   logic         rx,
  
  output  logic [ 7: 0] q,
  output  logic         valid
);

  logic [ 1: 0] r_pipe = '0;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      r_pipe <= '0;
    else
      r_pipe <= { r_pipe[0], rx };
      
  logic rx_edge;
  always_comb
    rx_edge = r_pipe[0] ^ r_pipe[1];
    
  logic rx_value;
  always_comb
    rx_value = r_pipe[0];

  localparam PERIOD_CLKS     = int'(cdivr(CLKFREQ, BITRATE    ));
  localparam SAMPLE_CLKS     = int'(cdivr(CLKFREQ, BITRATE * 2));

  localparam _CSCW = $clog2(PERIOD_CLKS);
  typedef logic [_CSCW-1:0] pcnt_t;
  
  pcnt_t    sp_cnt = '0;

  logic sp_ena;
  logic sp_cnt_wrap;
  
  always_comb begin
    sp_ena      = (sp_cnt == SAMPLE_CLKS - 1) && !rx_edge;
    sp_cnt_wrap = (sp_cnt == PERIOD_CLKS - 1);
  end
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      sp_cnt <= '0;
    else begin
      if (rx_edge || sp_cnt_wrap)
        sp_cnt <= '0;
      else
        sp_cnt <= sp_cnt + 1'b1;
    end
    
  logic [ 7: 0] shiftreg = '0;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      shiftreg <= '0;
    else
      if (sp_ena)
        shiftreg <= { rx_value, shiftreg[ 7: 1] };

  logic [ 3: 0] bc = '0;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      bc <= '0;
    else
      if (sp_ena) begin
        if (bc == '0) begin
          if (!rx_value )
            bc <= 4'd9;
        end
        else
          bc <= bc - 1'b1;
      end

  logic r_valid = '0;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      r_valid <= '0;
    else
      r_valid <= ((bc == 2) && sp_ena);
  
  always_comb begin
    q     = shiftreg;
    valid = r_valid;
  end

  function integer cdivr(input integer a, input integer b);
    cdivr = (a + (b / 2)) / b;
  endfunction

endmodule
