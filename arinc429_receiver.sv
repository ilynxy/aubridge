module arinc429_receiver
#(
  parameter N = 8
)
(
  input  logic          areset,
  input  logic          clk,

  input  logic          ena_mes1t,
  input  logic          ena_mes4t,

  input  wire [N-1: 0]  rx_a,
  input  wire [N-1: 0]  rx_b,
  
  output wire [N-1: 0]  rx_activity,
  
  input  logic          ws_busy,
  output logic          ws_ena,
  output logic [31: 0]  ws_d
);

  logic [N-1: 0][31:0] a429_q;
  logic [N-1: 0]       a429_qv;
  logic [N-1: 0]       a429_cv;

  genvar i;
  generate
    for (i = 0; i < N; ++i) begin : a429_rcv
      a429_deserializer
        rcv
        (
          .areset   (areset),
          .clk      (clk),
          
          .ena_mes1t  (ena_mes1t),
          .ena_mes4t  (ena_mes4t),

          .rx_a     (rx_a[i]),
          .rx_b     (rx_b[i]),

          .q        (a429_q[i]),
          .valid    (a429_qv[i]),
          .flush    (a429_cv[i])
        );

    end : a429_rcv
  endgenerate
  
  assign rx_activity = a429_qv;
  
  a429_queuer#( .N ( N ) )
    queuer
    (
      .areset       (areset),
      .clk          (clk),

      .a429_q       (a429_q),
      .a429_qv      (a429_qv),
      .a429_cv      (a429_cv),

      .ws_busy      (ws_busy),
      .ws_ena       (ws_ena),
      .ws_d         (ws_d)
    );

endmodule
