module frame_packetizer
#(
  parameter EN = 3
)
(
  input   logic         areset,
  input   logic         clk,
  
  input   logic [EN-1: 0][31: 0]  einfo,
  input   logic                   einfo_req,
  
  input   logic         commit,

  input   logic [ 7: 0] writes_count,

  input   logic [ 7: 0] rs_words,

  output  logic         rs_ena,
  input   logic         rs_busy,
  input   logic [31: 0] rs_q,
  
  input   logic         ser_ena,
  output  logic [ 7: 0] ser_d,
  output  logic         ser_load,
  input   logic         ser_busy
);

  localparam  SOF_MARKER = 8'h55;

  typedef enum logic [ 4: 0]
  {
    stIDLE, // wait for commit event
    stSEND_SOF,  // SOF
    stSEND_FRAME_NUMBER,  // frame number
    stSEND_WRITES_COUNT,
    stSEND_NUMBER_OF_WORDS,  // number of words
    stCHECK_COUNTER,
    stRDFIFO_L,
    stLATCH_L,
    stSEND_LINE_NUMBER,
    stRDFIFO_H,
    stLATCH_H,
    stSEND_DATA_A,
    stSEND_DATA_B,
    stSEND_DATA_C,
    stSEND_DATA_D,
    stSEND_EXT_INFO,
    stSEND_CRC   // send CRC8
  } state_t;

  state_t state, next_state;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      state <= stIDLE;
    else
      state <= next_state;
  
  logic         latch_fifo_data;
  logic [31: 0] fifo_data;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      fifo_data <= '0;
    else
      if (latch_fifo_data)
        fifo_data <= rs_q;
  
  logic [ 7: 0] number_of_frame;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      number_of_frame <= 0;
    else
      if (commit)
        number_of_frame <= number_of_frame + 1'b1;
  
  logic send_extended_info;
//  always_comb
//    send_extended_info = (number_of_frame == '0);

  always_ff@(posedge areset or posedge clk)
    if (areset)
      send_extended_info <= '0;
    else begin
      if      (state == stSEND_EXT_INFO)
        send_extended_info <= '0;
      else if (einfo_req)
        send_extended_info <= '1;
    end

  localparam EXT_INFO_LINE = 8'hFF;
  logic [14: 0][ 7: 0]  extinfo_array;
  always_comb begin
    extinfo_array[ 0] = EXT_INFO_LINE;
    extinfo_array[ 1] = einfo[0][ 7: 0];
    extinfo_array[ 2] = einfo[0][15: 8];
    extinfo_array[ 3] = einfo[0][23:16];
    extinfo_array[ 4] = einfo[0][31:24];
    
    extinfo_array[ 5] = EXT_INFO_LINE;
    extinfo_array[ 6] = einfo[1][ 7: 0];
    extinfo_array[ 7] = einfo[1][15: 8];
    extinfo_array[ 8] = einfo[1][23:16];
    extinfo_array[ 9] = einfo[1][31:24];
    
    extinfo_array[10] = EXT_INFO_LINE;
    extinfo_array[11] = einfo[2][ 7: 0];
    extinfo_array[12] = einfo[2][15: 8];
    extinfo_array[13] = einfo[2][23:16];
    extinfo_array[14] = einfo[2][31:24];
  end
  
  logic         extinfo_index_ena;
  logic [ 3: 0] extinfo_index_d;
  logic [ 3: 0] extinfo_index;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      extinfo_index = '0;
    else begin
      if (extinfo_index_ena)
        extinfo_index = extinfo_index_d;
    end
    
  logic [ 7: 0] extinfo_data;
  always_comb
    extinfo_data = extinfo_array[extinfo_index];

  logic [ 7: 0] number_of_words;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      number_of_words <= '0;
    else begin
      if (commit)
        number_of_words <= rs_words;
      else
        number_of_words <= number_of_words - (state == stCHECK_COUNTER);
    end
  
  logic [ 7: 0] frame_crc;
  
  always_comb begin
    next_state  = state;
    
    ser_d       = 'x;
    ser_load    = '0;
    
    latch_fifo_data = '0;
    
    rs_ena      = '0;
    
    extinfo_index_ena = '0;
    extinfo_index_d   = 'x;
    
    case (state)
      stIDLE:
        if (commit)
          next_state = stSEND_SOF;
      
      stSEND_SOF: begin
        ser_d     = SOF_MARKER;
        ser_load  = ser_ena && !ser_busy;
        if (ser_load)
          next_state = stSEND_FRAME_NUMBER;
      end
    
      stSEND_FRAME_NUMBER: begin
        ser_d     = number_of_frame[ 7: 0];
        ser_load  = ser_ena && !ser_busy;
        if (ser_load)
          next_state = stSEND_WRITES_COUNT;
      end

      stSEND_WRITES_COUNT: begin
        ser_d     = writes_count;
        ser_load  = ser_ena && !ser_busy;
        if (ser_load)
          next_state = stSEND_NUMBER_OF_WORDS;
      end
      
      stSEND_NUMBER_OF_WORDS: begin
        if (send_extended_info) begin
          extinfo_index_ena = '1;
          extinfo_index_d = '0;
        end
        
        ser_d     = number_of_words + (send_extended_info ? 8'd3 : 8'd0);
        ser_load  = ser_ena && !ser_busy;
        if (ser_load)
          next_state = stCHECK_COUNTER;
      end
      
      stCHECK_COUNTER: begin
        if (number_of_words == 0) begin
          if (send_extended_info)
            next_state = stSEND_EXT_INFO;
          else
            next_state = stSEND_CRC;
        end
        else
          next_state = stRDFIFO_L;
      end
      
      stRDFIFO_L: begin
        rs_ena = '1;
        if (!rs_busy)
          next_state = stLATCH_L;
      end
      
      stLATCH_L: begin
        latch_fifo_data = '1;
        next_state = stSEND_LINE_NUMBER;
      end
        
      stSEND_LINE_NUMBER: begin
        ser_d     = fifo_data[ 7: 0];
        ser_load  = ser_ena && !ser_busy;
        if (ser_load)
          next_state = stRDFIFO_H;
      end

      stRDFIFO_H: begin
        rs_ena = '1;
        if (!rs_busy)
          next_state = stLATCH_H;
      end
      
      stLATCH_H: begin
        latch_fifo_data = '1;
        next_state = stSEND_DATA_A;
      end

      stSEND_DATA_A: begin
        ser_d     = fifo_data[ 7: 0];
        ser_load  = ser_ena && !ser_busy;
        if (ser_load)
          next_state = stSEND_DATA_B;
      end

      stSEND_DATA_B: begin
        ser_d     = fifo_data[15: 8];
        ser_load  = ser_ena && !ser_busy;
        if (ser_load)
          next_state = stSEND_DATA_C;
      end

      stSEND_DATA_C: begin
        ser_d     = fifo_data[23:16];
        ser_load  = ser_ena && !ser_busy;
        if (ser_load)
          next_state = stSEND_DATA_D;
      end

      stSEND_DATA_D: begin
        ser_d     = fifo_data[31:24];
        ser_load  = ser_ena && !ser_busy;
        if (ser_load)
          next_state = stCHECK_COUNTER;
      end

      stSEND_CRC: begin
        ser_d     = frame_crc;
        ser_load  = ser_ena && !ser_busy;
        if (ser_load)
          next_state = stIDLE;
      end

      stSEND_EXT_INFO: begin
        ser_d     = extinfo_data;
        ser_load  = ser_ena && !ser_busy;
        
        if (ser_load) begin
          extinfo_index_ena = '1;
          extinfo_index_d   = extinfo_index + 1'b1;
          if (extinfo_index == 4'd14)
            next_state = stSEND_CRC;
        end
      end
  
    endcase
  
  end
  
  // CRC-8-CCITT x^8 + x^2 + x + 1 (ATM HEC), ISDN Header Error Control and Cell Delineation ITU-T I.432.1 (02/99)
  // http://www.itu.int/rec/T-REC-I.432.1-199902-I/en
  // https://ru.wikipedia.org/wiki/%D0%A6%D0%B8%D0%BA%D0%BB%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%B8%D0%B7%D0%B1%D1%8B%D1%82%D0%BE%D1%87%D0%BD%D1%8B%D0%B9_%D0%BA%D0%BE%D0%B4

  generic_crc#(
    .CRC_WIDTH     (8),
    .CRC_POLYNOME  (8'h07),
    .CRC_INITIAL   (8'hFF),
    .CRC_XOR       (8'h00),

    .DATA_WIDTH    (8),

    .DATA_REFLECT  (0),
    .CRC_REFLECT   (0)
  )
    crc8_ccitt
    (
      .reset_n      (!areset),
      .clk          (clk),

      .init         (commit),
      .d            (ser_d),
      .ena          (ser_load),

      .q            (frame_crc)
    );


endmodule
