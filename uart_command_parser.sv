module uart_command_parser
(
  input   logic         areset,
  input   logic         clk,
  
  input   logic [ 7: 0] deser_q,
  input   logic         deser_valid,
  output  logic         deser_acq,
  
  output  logic         player_ena,
  
  output  logic [ 7: 0] writes_count,
  
  output  logic         ram_ena,
  input   logic         ram_busy,
  output  logic [18: 0] ram_a,
  output  logic [ 3: 0] ram_be,
  output  logic [31: 0] ram_d,
  output  logic         ram_we,
  input   logic [31: 0] ram_q,
  
  output  logic [ 3: 0] chupd_q,
  input   logic [ 3: 0] chupd_acq,
  
  output  logic         einfo_req
);

  localparam VAR_OFFS     = 19'h02000;
  localparam SOF_MARKER   = 8'h55;
  // Test with SOF = 0xAA
  // localparam SOF_MARKER   = 8'hAA;

  typedef enum logic [ 3: 0]
  {
    stINITIALIZE_VARS,

    stWAIT_SOF,
    stOFFLEN,
    stOFFLEN_CRC,
    stCHECK_LENGTH,
    stDATA,
    stDATA_CRC,
    stWRITE
  } state_t;

  state_t state = stINITIALIZE_VARS;
  state_t next_state;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      state <= stINITIALIZE_VARS;
    else
      state <= next_state;
  
  logic [2: 0] icnt = '0;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      icnt <= '0;
    else
      if ( (state == stINITIALIZE_VARS) && !(ram_busy) )
        icnt <= icnt + 1'b1;
        
  logic [ 1: 0] bc = '0;
  logic reset_bc;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      bc <= '0;
    else begin
      if (reset_bc)
        bc <= '0;
      else
        if (deser_acq)
          bc <= bc + 1'b1;
    end

  logic [31: 0] data   = '0;
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      data   <= '0;
    end
    else
      if ( ((state == stOFFLEN) || (state == stDATA)) && deser_acq )
        data <= { deser_q, data[31: 8] };
  
  logic [23: 0] offset = '0;
  logic latch_offset;
  logic increment_offset;
  
  logic [ 7: 0] length = '0;
  logic latch_length;
  logic decrement_length;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      offset <= '0;
      length <= '0;
    end
    else begin
      if (latch_offset)
        offset <= data[31: 8];
      else
        if (increment_offset)
          offset <= offset + 1'b1;
      
      if (latch_length)
        length <= deser_q;
      else
        if (decrement_length)
          length <= length - 1'b1;
    end

  // CRC-8-CCITT x^8 + x^2 + x + 1 (ATM HEC), ISDN Header Error Control and Cell Delineation ITU-T I.432.1 (02/99)
  // http://www.itu.int/rec/T-REC-I.432.1-199902-I/en
  // https://ru.wikipedia.org/wiki/%D0%A6%D0%B8%D0%BA%D0%BB%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%B8%D0%B7%D0%B1%D1%8B%D1%82%D0%BE%D1%87%D0%BD%D1%8B%D0%B9_%D0%BA%D0%BE%D0%B4

  logic reset_crc;
  logic [ 7: 0] crc;
  
  generic_crc#(
    .CRC_WIDTH     (8),
    .CRC_POLYNOME  (8'h07),
    .CRC_INITIAL   (8'hFF),
    .CRC_XOR       (8'h00),

    .DATA_WIDTH    (8),

    .DATA_REFLECT  (0),
    .CRC_REFLECT   (0)
  )
    crc8_ccitt
    (
      .reset_n      (!areset),
      .clk          (clk),

      .init         (reset_crc),
      .d            (deser_q),
      .ena          (deser_acq),

      .q            (crc)
    );

  logic [ 7: 0] wcnt = '0;
  logic writes_count_increment;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      wcnt <= '0;
    else
      if (writes_count_increment)
        wcnt <= wcnt + 1'b1;
   
  always_comb
    writes_count = wcnt;
    
  // 'write monitor' logic
  logic [ 3: 0] chupd_q_reg;
  logic [ 3: 0] chupd_q_set;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      chupd_q_reg <= '0;
    else begin
/*
      for (int i = 0; i < 4; ++ i) begin
        if      (chupd_q_set[i])
          chupd_q_reg <= '1;
        else if (chupd_acq[i])
          chupd_q_reg <= '0;
      end
*/
      chupd_q_reg <= (chupd_q_reg & ~chupd_acq) | chupd_q_set;
    end

  always_comb begin
  
    next_state = state;
  
    ram_ena = '0;
    ram_a   = 'x;
    ram_be  = '1;
    ram_d   = 'x;
    ram_we  = '0;
    
    reset_bc = '0;
    reset_crc = '0;
    
    deser_acq = '0;
    
    latch_length = '0;
    decrement_length = '0;
    latch_offset = '0;
    increment_offset = '0;
    
    writes_count_increment = '0;
    
    chupd_q_set = '0;
    
    einfo_req = '0;
    
    case (state)
    stINITIALIZE_VARS: begin
      ram_ena = '1;
      ram_a   = VAR_OFFS + icnt;
      ram_we  = '1;
      ram_d   = '0;
      if (icnt == '1)
        next_state = stWAIT_SOF;
    end
    
    stWAIT_SOF: begin
      if (deser_valid) begin
        deser_acq = '1;

        if (deser_q == SOF_MARKER) begin
          reset_bc  = '1;
          next_state = stOFFLEN;
        end
        else
          reset_crc = '1;
      end
    end

    stOFFLEN: begin
      if (deser_valid) begin
        deser_acq = '1;
        if (bc == 3) begin
          next_state = stOFFLEN_CRC;
          latch_length = '1;
          latch_offset = '1;
        end
      end
    end

    stOFFLEN_CRC: begin
      if (deser_valid) begin
        deser_acq = '1;

        reset_bc   = '1;
        reset_crc  = '1;

        if (deser_q == crc) begin
          if (length == '0) begin
            next_state = stWAIT_SOF;
            einfo_req = '1;
          end
          else begin
            next_state = stDATA;
          end
        end
        else
          next_state = stWAIT_SOF;
      end
    end

    stDATA: begin
      if (deser_valid) begin
        deser_acq = '1;
        if (bc == 3)
          next_state = stDATA_CRC;
      end
    end
    
    stDATA_CRC: begin
      if (deser_valid) begin
        deser_acq = '1;
        
        if (deser_q == crc) begin
          next_state = stWRITE;
          reset_bc   = '1;
          reset_crc  = '1;
          decrement_length = '1;
        end
        else
          next_state = stWAIT_SOF;
      end
    end

    stWRITE: begin
      ram_ena = '1;
      ram_a   = offset[18: 0];
      ram_we  = '1;
      ram_d   = data;
      
      if (!ram_busy) begin
        next_state = (length == '0) ? stWAIT_SOF : stDATA;
        increment_offset = '1;
        writes_count_increment = '1;
        
        // Write to player area monitor to catch update of 'pause/length' register
        if ( { offset[18: 0] >> 3, offset[0] } == { 18'h02000 >> 3, 1'b1 } ) begin
          chupd_q_set[offset[2:1]] = '1;
        end
      end

    end
    
    endcase
  end
  
  always_comb
    player_ena = (state != stINITIALIZE_VARS);
    
  always_comb begin
    chupd_q = chupd_q_reg;
  end

endmodule
