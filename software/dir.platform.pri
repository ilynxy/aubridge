_OUTDIRPFX = "../.bin/"
_INTDIRPFX = "../.bin/.cache/"

linux {

  linux-g++     : QMAKE_TARGET.arch = $$QMAKE_HOST.arch
  linux-clang   : QMAKE_TARGET.arch = $$QMAKE_HOST.arch
  linux-g++-32  : QMAKE_TARGET.arch = x86
  linux-g++-64  : QMAKE_TARGET.arch = x86_64

  CONFIG(debug, debug|release) {
    _CONFIGURATION = "debug"
    DEFINES += _DEBUG
  } else {
    _CONFIGURATION = "release"
    DEFINES += _RELEASE
  }

  _PLATFORM = $${QMAKE_TARGET.arch}
}

win32 {
  unset(MSYS2)
  contains(QMAKESPEC, .*msys.*mingw64.*) {
    QMAKE_TARGET.arch = x86_64
    MSYS2 = "MSYS2_x86_64"
  }
  contains(QMAKESPEC, .*msys.*mingw32.*) {
    QMAKE_TARGET.arch = x86
    MSYS2 = "MSYS2_x86"
  }

  CONFIG(debug, debug|release) {
    _CONFIGURATION = "Debug"
    DEFINES += _DEBUG
  } else {
    _CONFIGURATION = "Release"
    DEFINES += _RELEASE
  }

  !contains(QMAKE_TARGET.arch, x86_64) {
      _PLATFORM = "Win32"
  } else {
      _PLATFORM = "x64"
  }

}

!defined(MSYS2, var) {
  msvc        : _TOOLSET = msvc
  win32-g++   : _TOOLSET = mingw
  linux-g++*  : _TOOLSET = gcc
  linux-clang : _TOOLSET = clang
} else {
  _TOOLSET = msys
}

#build_pass {
#  message(mkspec: $${QMAKESPEC})
#  message(Platform: $${_PLATFORM})
#  message(Configuration: $${_CONFIGURATION})
#  message(MSYS2 detect: $${MSYS2});
#  message(TOOLSET: $${_TOOLSET})
#}

_OUTDIR = $${_OUTDIRPFX}$${_TOOLSET}.$${_CONFIGURATION}.$${_PLATFORM}
_INTDIR = $${_INTDIRPFX}$${TARGET}/$${_TOOLSET}.$${_CONFIGURATION}.$${_PLATFORM}

DESTDIR = $${_OUTDIR}

OBJECTS_DIR = $${_INTDIR}
MOC_DIR = $${_INTDIR}
UI_DIR  = $${_INTDIR}
RCC_DIR = $${_INTDIR}
PRECOMPILED_DIR = $${_INTDIR}
