BOOST_LIBSUFFIX = ""

win32 {
  BOOST_ROOT    = $$quote(../boost)
  BOOST_VERSION = 1_57

  BOOST_INCLUDEPATH = $$quote($${BOOST_ROOT}/include/boost-$${BOOST_VERSION})
  BOOST_LIBPATH     = $$quote($${BOOST_ROOT}/lib)

  contains(QMAKE_TARGET.arch, x86_64) {
    BOOST_BUILDID=amd64
  } else {
    BOOST_BUILDID=i686
  }

  win32-g++:      BOOST_TOOLSETID = mgw49-mt
  win32-msvc2013: BOOST_TOOLSETID = vc120-mt
  win32-msvc:     DEFINES += BOOST_LIB_BUILDID=$${BOOST_BUILDID}

  CONFIG(debug, debug|release) {
    BOOST_DEBUGLIBID = -d
  } else {
    BOOST_DEBUGLIBID =
  }

  !defined(MSYS2, var) {
    INCLUDEPATH += $${BOOST_INCLUDEPATH}
    LIBS        += -L$${BOOST_LIBPATH}
    BOOST_LIBSUFFIX = -$${BOOST_TOOLSETID}$${BOOST_DEBUGLIBID}-$${BOOST_VERSION}-$${BOOST_BUILDID}.dll
  } else {
    BOOST_LIBSUFFIX = -mt
  }

#  build_pass {
#    message(BOOST_INCLUDEPATH: $${BOOST_INCLUDEPATH})
#    message(BOOST_LIBPATH: $${BOOST_LIBPATH})
#    message(BOOST_BUILDID: $${BOOST_BUILDID})
#    message(BOOST_LIBSUFFIX: $${BOOST_LIBSUFFIX})
#  }

  DEFINES += BOOST_REGEX_NO_LIB BOOST_DATE_TIME_NO_LIB
}

win32-g++ {
#  DEFINES += _WIN32_WINNT=0x0600 _UNICODE
}

unix {
}

DISTFILES +=

