#include <unistd.h>
#include <fcntl.h>
#include <linux/serial.h>
#include <termios.h>
#include <errno.h>
#include <sys/eventfd.h>
#include <poll.h>
#include <string.h>

#include <iostream>
#include <thread>
#include <atomic>

#define FMT_HEADER_ONLY
#include "fmt/printf.h"

#include "linear_buffer.hpp"
#include <boost/crc.hpp>
#include <boost/endian/arithmetic.hpp>

std::atomic<uint8_t> g_NOW;

class rxparser
{
public:
  typedef std::uint64_t   counter_t;
  typedef unsigned char   char_t;

protected:

  const size_t  linear_buffer_size_;
  linear_buffer lb_;

  bool          synchronized_;
  std::uint8_t  seq_;

  counter_t     skipped_;
  counter_t     processed_;

  bool is_SOF(char_t c) const
  {
    return (c == 0x55);
  }

  enum class result_t
  {
    valid,
    invalid,
    need_more_data,
    bad_checksum
  };

  result_t is_valid_packet(const char_t *p, std::size_t size, std::size_t& packet_size)
  {
    packet_size = 1;

    if (size < 5)
      return result_t::need_more_data;

    // boost::crc_basic<8>  crc(0x07, 0xFF, 0, false, false);
    boost::crc_optimal<8, 0x07, 0xFF, 0, false, false> crc;

    char_t b;

    b = *p ++;
    -- size;

    if (!is_SOF(b))
      return result_t::invalid;

    crc.process_byte(b);

    crc.process_byte(*p ++); // skip frame seq number
    crc.process_byte(*p ++); // skip number of writes
    size -= 2;

    b = *p ++;
    -- size;

    std::size_t nAW = b; // number of ARINC words;

    if (nAW > (4 * 8) ) // maximum 4 words per channel x 8 channels per frame
      return result_t::invalid;
    crc.process_byte(b);


    std::size_t dsize = (nAW * 5) + 1; // 5 bytes per ARINC word + 1 byte CRC8

    if (size < dsize)
      return result_t::need_more_data;

    for (std::size_t i = 0; i < dsize; ++ i)
      crc.process_byte(*p ++);

    if (crc.checksum() != 0)
      return result_t::bad_checksum;

    packet_size = 4 + dsize; // (SOF + SEQ + NOW + NAW) + ((5 * NAW) + CRC)

    return result_t::valid;
  }

  void sync_event() const
  {
    //boost::format sync_fmt("%08X: synchronized");
    //std::cout << sync_fmt % offset_bytes() << '\n';
    fmt::printf("%08X: synchronized\n", offset_bytes());
  }

  void desync_event() const
  {
    //boost::format desync_fmt("%08X: desynchronized");
    //std::cout << desync_fmt % offset_bytes() << '\n';
    fmt::printf("%08X: desynchronized\n", offset_bytes());
  }

  void dump_valid_packet(const char_t *p)
  {

    //boost::format header("%08X: S,%3u,%3u,%3u [");
    //std::string s;

    unsigned sof = *p++; (void)(sof); // TODO: Get rid of unused variable
    unsigned seq = *p++;
    unsigned now = *p++;
    unsigned naw = *p++;

    g_NOW.store(now);

    //s = str(header % offset_bytes() % seq % now % naw);

    //boost::format arinc(" %u:%02X%02X%02X%02X");

    for (std::size_t i = 0; i < naw; ++ i)
    {
      unsigned nl = *p++;
      unsigned b3 = *p++;
      unsigned b2 = *p++;
      unsigned b1 = *p++;
      unsigned b0 = *p++;

      //if (nl == 0xFF)
      {
      //s += str(arinc % nl % b0 % b1 % b2 % b3);
          //fmt::printf("Extended info: Line = %u, Addr = %03o, [2,1,0] = %u,%u,%u\n", nl, b3, b2, b1, b0);
          if (b3 == 000) // Version info
              fmt::printf("Extended info: Version %u.%u (ARINC-429 address is %sreversed)\n", b1, b2, b0 != 0 ? "" : "NOT ");

          if (b3 == 0201 || b3 == 0102) // Temp sensor
          {
              unsigned temp = (b1 << 8) | b2;
              fmt::printf("Extended info: Temp sensor %u = ", (b3 & 0x0F) );
              if (temp == 0x8000) // unknown temp
                fmt::printf("unknown\n");
              else
                fmt::printf("%3.3f C\n", temp / 16.0);
          }
      }
    }

    unsigned crc = *p ++;

    //boost::format crcfmt(" ],%02X");
    //s += str(crcfmt % crc);

    if (seq != seq_)
    {

      //s += "(??SEQ)";
      fmt::printf("Frame sequence broken. Expected: %u, got: %u\n", seq_, seq);

      seq_ = seq;
    }

    ++ seq_;

    //std::cout << s << '\n';

  }

  std::size_t process_buffer(const char *data, std::size_t size)
  {
    const char_t *p = reinterpret_cast<const char_t *>(data);
    std::size_t    residue = size;

    for(;;)
    {
      std::size_t packet_size;
      result_t result = is_valid_packet(p, residue, packet_size);

      if (result == result_t::need_more_data)
        break;

      if (result == result_t::valid)
      {
        if (!synchronized_)
        {
          synchronized_ = true;
          seq_ = p[1];
          sync_event();
        }

        dump_valid_packet(p);

        p       += packet_size;
        residue -= packet_size;

        processed_ += packet_size;
      }
      else
      {
        if (synchronized_)
        {
          synchronized_ = false;
          desync_event();
        }

        p       += 1;
        residue -= 1;

        skipped_ += 1;
      }
    }

    return (size - residue);
  }

public:

  rxparser() :
    linear_buffer_size_(65536),
    lb_(linear_buffer_size_),
    synchronized_(false),
    seq_(0),
    skipped_(0),
    processed_(0)
  {
  }

  ~rxparser()
  {
  }

  void process_next_chunk(const char *data, std::size_t size)
  {
    do
    {
      auto pushed = lb_.push_back(static_cast<const char *>(data), size);

      data += pushed;
      size -= pushed;

      std::size_t processed = process_buffer(lb_.linearize(), lb_.size());

      auto popped = lb_.pop_front(processed);
      (void)(popped); // TODO: Get rid of unused variable

    } while (size != 0);
  }

  void start(counter_t skipped = 0)
  {
    synchronized_ = false;
    skipped_      = skipped;
    processed_    = 0;
  }

  void stop()
  {
    //auto ub = unprocessed_bytes();
    //std::cout << "Unprocessed: " << ub << '\n';
    fmt::printf("Processed bytes: %u\n", processed_bytes());
    fmt::printf("Unprocessed bytes: %u\n", unprocessed_bytes());
    fmt::printf("Skipped bytes: %u\n", skipped_bytes());
  }

  counter_t skipped_bytes() const
  {
    return skipped_;
  }

  counter_t processed_bytes() const
  {
    return processed_;
  }

  counter_t unprocessed_bytes() const
  {
    return lb_.size();
  }

  counter_t offset_bytes() const
  {
    return (skipped_bytes() + processed_bytes());
  }
};



typedef int native_handle_t;

void read_thread(native_handle_t fd_input_pipe, native_handle_t fd_stop_event)
{
  std::cout << "Read thread started..." << std::endl;

  ::pollfd fds[2];

  fds[0].fd     = fd_input_pipe;
  fds[0].events = POLLIN;
  fds[1].fd     = fd_stop_event;
  fds[1].events = POLLIN;

  int poll_timeout = -1; // infinite

  int status;

  rxparser rxp;

  rxp.start();

  for (;;)
  {
    status = ::poll(fds, 2, poll_timeout);
    if (status < 0)
    {
      status = errno;

      std::cout << "::poll(...) failed: " << status << std::endl;
      break;
    }

    if (fds[0].revents & POLLIN)
    {
      // something received
      const size_t buffer_size = 4096;
      char buffer[buffer_size];

      ssize_t rsize = ::read(fds[0].fd, buffer, buffer_size);
      if (rsize < 0)
      {
        status = errno;
        std::cout << "::read(fd_input_pipe, ...) failed: " << status << std::endl;

        break;
      }

      rxp.process_next_chunk(buffer, rsize);

    }

    if (fds[1].revents & POLLIN)
    {
      // break event;
      std::cout << "stop_event signaled. exit read loop." << std::endl;
      break;
    }
  }

  rxp.stop();

  std::cout << "Read thread exit..." << std::endl;
}

int write_to_pipe(native_handle_t fd_output_pipe, const void *data, size_t size)
{
  int status = 0;

  const char *p = static_cast<const char *>(data);
  while (size != 0)
  {
    ssize_t written = ::write(fd_output_pipe, p, size);
    if (written < 0)
    {
      status = written;
      fmt::printf("::write(fd_output_pipe, ...) failed: %d\n", errno);
      break;
    }

    p += static_cast<size_t>(written);
    size -= static_cast<size_t>(written);
  }

  return status;
}

int write_memblock(native_handle_t fd_output_pipe, uint32_t offset, std::vector<uint32_t> data)
{
  int status;

  boost::crc_optimal<8, 0x07, 0xFF, 0, false, false> crc;
  typedef boost::endian::little_uint8_t le_uint8_t;
  typedef boost::endian::little_uint32_t le_uint32_t;

  struct write_memblock_header
  {
    le_uint8_t  sof;
    le_uint32_t offset;
    le_uint8_t  checksum;
  };

  struct write_memblock_data
  {
    le_uint32_t data;
    le_uint8_t  checksum;
  };

  uint32_t length = static_cast<uint32_t>(data.size());

  write_memblock_header hdr;
  hdr.sof       = 0x55;
  hdr.offset    = (offset & 0x00FFFFF) | ((length & 0x000000FF) << 24);
  crc.reset();
  crc.process_bytes(&hdr, sizeof(hdr) - sizeof (hdr.checksum));
  hdr.checksum  = crc.checksum();

  status = write_to_pipe(fd_output_pipe, &hdr, sizeof(hdr));
  if (status != 0)
    return status;

  for (auto d : data)
  {
    write_memblock_data v;
    v.data = d;
    crc.reset();
    crc.process_bytes(&v, sizeof(v) - sizeof(v.checksum));
    v.checksum = crc.checksum();
    status = write_to_pipe(fd_output_pipe, &v, sizeof(v));
  }

  return status;
}

int main()
{
  int status;

  const char *serial_port_name = "/dev/ttyUSB0";
  int fdPort = ::open(serial_port_name, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (fdPort == -1)
  {
    status = errno;
  }

  ::termios options;
  status = ::tcgetattr(fdPort, &options);
  if (status == -1)
    status = errno;

  ::speed_t baud_rate = B2000000;

  status = ::cfsetspeed(&options, baud_rate);
  if (status == -1)
    status = errno;

  ::cfmakeraw(&options);

  status = ::tcsetattr(fdPort, TCSANOW, &options);
  if (status == -1)
    status = errno;

  int fdBreak = ::eventfd(0, EFD_NONBLOCK);
  if (fdBreak == -1)
  {
    status = errno;
  }


  std::thread rt(read_thread, fdPort, fdBreak);

  std::default_random_engine rand_gen_{std::random_device{}()};

  // set all channels to maximum speed
  const std::vector<uint32_t> config =
  {
    0x00000000, 0x00000010, // channel #1: speed = 0, offset = 0x00000000, length = 2
    0x00000000, 0x00000010, // channel #2: speed = 0, offset = 0x00000000, length = 2
    0x00000000, 0x00000010, // channel #3: speed = 0, offset = 0x00000000, length = 2
    0x00000000, 0x00000010  // channel #4: speed = 0, offset = 0x00000000, length = 2
    //0x01000001, 0x00000003, // channel #2: speed = 1, offset = 0x00000001, length = 3
    //0x02000002, 0x00000004, // channel #3: speed = 2, offset = 0x00000002, length = 4
    //0x03000003, 0x00000005  // channel #4: speed = 3, offset = 0x00000003, length = 5
  };

  write_memblock(fdPort, 0x00002000, config);


  std::cout << "main thread waits for 1 seconds..." << std::endl;
  ::sleep(1);


  std::vector<uint32_t> mem_data;

  for (size_t i = 0; i < 100; ++ i)
  {
    uint8_t now = g_NOW.load();
    fmt::printf("%u: NOW before write: %d\n", i, now);
    std::cout.flush();

    // send 'EMPTY' request
    {
      //const uint8_t clr[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
      //write_to_pipe(fdPort, clr, 8);
      mem_data.clear();
      write_memblock(fdPort, 0x00010000, mem_data);
    }

    uint8_t expected_now = now;
    size_t words_to_write;

    for (size_t j = 0; j < 0; ++ j)
    {
      words_to_write = 3; //std::uniform_int_distribution<size_t>{1, 10}(rand_gen_);

      mem_data.clear();
      mem_data.reserve(words_to_write);

      for (size_t k = 0; k < words_to_write; ++ k)
        //mem_data.push_back(std::uniform_int_distribution<uint32_t>{0, 0xFFFFFFFF}(rand_gen_));
        mem_data.push_back(0x12345678);

      fmt::printf("Write to mem %d words\n", words_to_write);
      write_memblock(fdPort, 0x00010000, mem_data);

      expected_now += words_to_write;
    }

    //::sleep(1);
    ::usleep(500000);

    now = g_NOW.load();
    fmt::printf("%u: NOW after write: %d, expected %d%s\n", i, now, expected_now, (now == expected_now) ? "" : " ERRRRRROORRR!!!");
    std::cout.flush();
  }

/*
  ::pollfd fds[1];
  fds[0].fd = ::fileno(stdin);
  fds[0].events = POLLIN;

  for (;;)
  {
    ::poll(fds, 1, 1000);
    if (fds[0].revents & POLLIN)
      break;
  }
*/


  std::cout << "set stop_event in signaled state." << std::endl;
  uint64_t sv = 1;
  ssize_t written = ::write(fdBreak, &sv, sizeof(sv));
  if (written != sizeof(sv))
  {
  }

  if (status < 0)
    status = errno;

  std::cout << "wait for read thread completion..." << std::endl;
  rt.join();

  status = ::close(fdBreak);
  if (status == -1)
    status = errno;

  status = ::close(fdPort);
  if (status == -1)
    status = errno;


  return 0;
}
