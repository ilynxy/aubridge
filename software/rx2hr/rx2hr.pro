TEMPLATE = app
CONFIG  += console precompile_header
CONFIG  -= app_bundle
CONFIG  -= qt
CONFIG  += c++14
# CONFIG  += static

include(../dir.platform.pri)
# include(../boost.pri)

SOURCES += \
  rx2hr.cpp

HEADERS += \
    linear_buffer.hpp

QMAKE_CXXFLAGS += -std=c++14
