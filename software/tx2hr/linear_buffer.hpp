#ifndef LINBUFFER_H
#define LINBUFFER_H

#include <algorithm>

template<typename data_type, typename size_type>
class linear_buffer_dyn_templ
{
protected:
  data_type *const buffer_;
  size_type  const capacity_;

  size_type  size_;
  size_type  head_;

public:
  linear_buffer_dyn_templ(size_type capacity)
    : buffer_(new data_type[capacity])
    , capacity_(capacity)
    , size_(0)
    , head_(0)
  {
  }

  ~linear_buffer_dyn_templ()
  {
    delete[] buffer_;
  }

  size_type capacity() const
  {
    return capacity_;
  }

  size_type size() const
  {
    return size_;
  }

  size_type unused() const
  {
    return (capacity() - size());
  }

  size_type push_back(const data_type *data, size_type size)
  {
    size_type push_size = unused();
    if (push_size > size)
      push_size = size;

    if (head_ + size_ + push_size > capacity_)
    {
      std::uninitialized_copy_n(buffer_ + head_, size_, buffer_);
      // ::memmove(buffer_, buffer_ + head_, size_);
      head_ = 0;
    }

    std::uninitialized_copy_n(data, push_size, buffer_ + head_ + size_);
    // ::memcpy(buffer_ + head_ + size_, data, push_size);

    size_ += push_size;

    return push_size;
  }

  size_type pop_front(size_type size)
  {
    size_type pop_size = size_ > size ? size : size_;

    size_ -= pop_size;
    head_ += pop_size;

    return pop_size;
  }

  bool empty() const
  {
    return (size_ == 0);
  }

  void clear()
  {
    head_ = 0;
    size_ = 0;
  }

  const data_type * linearize() const
  {
    return (buffer_ + head_);
  }
};

typedef linear_buffer_dyn_templ<char, std::size_t> linear_buffer;

#endif
