TEMPLATE = app
CONFIG  += console precompile_header
CONFIG  -= app_bundle
CONFIG  -= qt
CONFIG  += static
CONFIG  += c++14

include(../dir.platform.pri)
# include(../boost.pri)

SOURCES += \
  tx2hr.cpp

HEADERS += \
    linear_buffer.hpp

QMAKE_CXXFLAGS += -std=c++14
