#include <fstream>
#include <iostream>
#include <string>
#include <memory>

#include <cstddef>
#include <cassert>

#include <boost/crc.hpp>
#include <boost/format.hpp>

#include "linear_buffer.hpp"

class txparser
{
public:
  typedef std::uint64_t   counter_t;
  typedef unsigned char   char_t;

protected:

  const size_t  linear_buffer_size_;
  linear_buffer lb_;

  bool          synchronized_;
  std::uint8_t  seq_;

  counter_t     skipped_;
  counter_t     processed_;

  bool is_SOF(char_t c) const
  {
    return (c == 0x55);
  }

  enum class result_t
  {
    valid,
    invalid,
    need_more_data,
    bad_checksum
  };

  result_t is_valid_packet(const char_t *p, std::size_t size, std::size_t& packet_size)
  {
    packet_size = 1;

    if (size < 6)
      return result_t::need_more_data;

    // boost::crc_basic<8>  crc(0x07, 0xFF, 0, false, false);
    boost::crc_optimal<8, 0x07, 0xFF, 0, false, false> crc;

    char_t b;

    b = *p ++; // SOF
    -- size;

    if (!is_SOF(b))
      return result_t::invalid;

    crc.process_byte(b);

    crc.process_byte(*p ++); // skip offset 7:0
    crc.process_byte(*p ++); // skip offset 15:8
    crc.process_byte(*p ++); // skip offset 23:16
    size -= 3;

    b = *p ++;
    -- size;
    std::size_t NOW = b; // number of WRITES;
    crc.process_byte(b);


    b = *p ++; // header CRC
    -- size;
    crc.process_byte(b);

    if (crc.checksum() != 0)
      return result_t::bad_checksum;

    std::size_t dsize = (NOW * 5); // 4 bytes per word + 1 byte CRC8

    if (size < dsize)
      return result_t::need_more_data;

    for (std::size_t i = 0; i < NOW; ++ i)
    {
      crc.reset();
      crc.process_byte(*p ++); // data 7:0
      crc.process_byte(*p ++); // data 15:8
      crc.process_byte(*p ++); // data 23:16
      crc.process_byte(*p ++); // data 31:24
      crc.process_byte(*p ++); // data CRC
      if (crc.checksum() != 0)
        break;
    }

    if (crc.checksum() != 0)
      return result_t::bad_checksum;

    packet_size = 6 + dsize; // (SOF + OFFSx3 + NOW + CRC) + (NOW * (DATAx4 + CRC))

    return result_t::valid;
  }

  void sync_event() const
  {
    boost::format sync_fmt("%08X: synchronized");
    std::cout << sync_fmt % offset_bytes() << '\n';
  }

  void desync_event() const
  {
    boost::format desync_fmt("%08X: desynchronized");
    std::cout << desync_fmt % offset_bytes() << '\n';
  }

  void dump_valid_packet(const char_t *p)
  {

    boost::format header("%08X: %02X,%02X%02X%02X,%u:%02X [");
    std::string s;

    unsigned sof = *p++; (void)(sof); // TODO: Get rid of unused variable
    unsigned o0  = *p++;
    unsigned o1  = *p++;
    unsigned o2  = *p++;
    unsigned now = *p++;
    unsigned crc = *p++;

    s = str(header % offset_bytes() % sof % o2 % o1 % o0 % now % crc);

    boost::format wordcrc(" %02X%02X%02X%02X:%02X");

    for (std::size_t i = 0; i < now; ++ i)
    {
      unsigned b3 = *p++;
      unsigned b2 = *p++;
      unsigned b1 = *p++;
      unsigned b0 = *p++;
      unsigned c  = *p++;

      s += str(wordcrc % b0 % b1 % b2 % b3 % c);
    }

    std::cout << s << " ]\n";
  }

  std::size_t process_buffer(const char *data, std::size_t size)
  {
    const char_t *p = reinterpret_cast<const char_t *>(data);
    std::size_t    residue = size;

    for(;;)
    {
      std::size_t packet_size;
      result_t result = is_valid_packet(p, residue, packet_size);

      if (result == result_t::need_more_data)
        break;

      if (result == result_t::valid)
      {
        if (!synchronized_)
        {
          synchronized_ = true;
          seq_ = p[1];
          sync_event();
        }

        dump_valid_packet(p);

        p       += packet_size;
        residue -= packet_size;

        processed_ += packet_size;
      }
      else
      {
        if (synchronized_)
        {
          synchronized_ = false;
          desync_event();
        }

        p       += 1;
        residue -= 1;

        skipped_ += 1;
      }
    }

    return (size - residue);
  }

public:

  txparser() :
    linear_buffer_size_(65536),
    lb_(linear_buffer_size_),
    synchronized_(false),
    seq_(0),
    skipped_(0),
    processed_(0)
  {
  }

  ~txparser()
  {
  }

  void process_next_chunk(const char *data, std::size_t size)
  {
    do
    {
      auto pushed = lb_.push_back(static_cast<const char *>(data), size);

      data += pushed;
      size -= pushed;

      std::size_t processed = process_buffer(lb_.linearize(), lb_.size());

      auto popped = lb_.pop_front(processed);
      (void)(popped); // TODO: Get rid of unused variable

    } while (size != 0);
  }

  void start(counter_t skipped = 0)
  {
    synchronized_ = false;
    skipped_      = skipped;
    processed_    = 0;
  }

  void stop()
  {
    auto ub = unprocessed_bytes();
    if (ub != 0)
      std::cout << "Unprocessed: " << ub << '\n';
  }

  counter_t skipped_bytes() const
  {
    return skipped_;
  }

  counter_t processed_bytes() const
  {
    return processed_;
  }

  counter_t unprocessed_bytes() const
  {
    return lb_.size();
  }

  counter_t offset_bytes() const
  {
    return (skipped_bytes() + processed_bytes());
  }
};


int main(int argc, char ** argv)
{

  if (argc != 2)
  {
    std::cerr << "Usage: " << argv[0] << " tx_binary_dump" << std::endl;
    return -1;
  }

  std::ifstream ifs(argv[1], std::ios::binary);
  if (!ifs.is_open())
  {
    std::cerr << "Cannot open file: " << argv[1] << std::endl;
    return -1;
  }

  txparser p;

  std::cout.sync_with_stdio(false);

  const std::size_t buffer_size = 65535;
  // auto buffer_allocated = std::make_unique<char[]>(buffer_size);
  auto buffer_allocated = std::unique_ptr<char[]>(new char[buffer_size]);``
  auto buffer = buffer_allocated.get();

  p.start();

  for (;;)
  {
    ifs.read(buffer, buffer_size);

    std::size_t transferred = ifs.gcount();

    p.process_next_chunk(buffer, transferred);

    if (ifs.eof() || ifs.fail())
      break;
  }

  p.stop();

  return 0;
}
