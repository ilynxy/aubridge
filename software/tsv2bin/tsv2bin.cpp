#include <fstream>
#include <iostream>

#include <boost/lexical_cast.hpp>

class uart
{
protected:
public:
  std::uint_least16_t   sh_;
  unsigned int          bc_;

  std::uint8_t          w_;

  unsigned int          pv_;

  bool                  bnew_;
  bool                  bvalid_;

public:
  uart() :
    sh_(0),
    bc_(0),
    w_(0),
    pv_(0),
    bnew_(false),
    bvalid_(false)
  {
  }

  ~uart()
  {
  }

  void parse_next_bit(unsigned int v)
  {
    v &= 1;

    sh_ >>= 1;
    sh_ |=  (v << 9);

    bnew_ = false;

    if (bc_ == 0)
    {
      if (v == 0 && pv_ == 1)
        bc_ = 9;
    }
    else
    {
      bc_ = bc_ - 1;

      if (bc_ == 0)
      {
        bnew_ = true;

        bvalid_ = false;
        if ( (sh_ & 0b1000000001) == 0b1000000000 )
          bvalid_ = true;

        w_ = static_cast<std::uint8_t>(sh_ >> 1);
      }
    }

    pv_ = v;
  }

};

template<typename IntegerType>
IntegerType extract_field_as_decimal(const char *&p, IntegerType val = 0)
{
  for(;;)
  {
    IntegerType v = (*p ^ static_cast<char>('0'));
    if (v > 9)
      break;

    ++ p;
    val = val * 10 + v;
  }

  return val;
}

bool extract_time_and_value(const char *p, double& time, unsigned& value)
{
  while ( std::isspace(*p) )
    ++ p;

  bool negate = (*p == '-');
  if (negate)
    ++ p;

  uint64_t v = extract_field_as_decimal(p, uint64_t(0));

  std::ptrdiff_t exp = 0;
  if (*p == '.')
  {
    const char *fracs = ++ p;
    v = extract_field_as_decimal(p, v);
    exp = p - fracs;
  }

  static double const exp_table[] = {
    1, 1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7,
    1e-8, 1e-9, 1e-10, 1e-11, 1e-12, 1e-13, 1e-14,
    1e-15, 1e-16, 1e-17, 1e-18, 1e-19, 1e-20, 1e-21
  };

  time = v * exp_table[exp];
  if (negate)
    time = -time;

  while ( std::isspace(*p) )
    ++ p;

  value = extract_field_as_decimal(p, unsigned(0));
  return true;
}

int main(int argc, char **argv)
{
  if (argc < 4)
  {
    std::cerr << "Usage: " << argv[0] << " tsv_dump bin_line0 bin_line1" << std::endl;
    return -1;
  }
  std::ifstream ifs(argv[1]);
  if (!ifs.is_open())
  {
    return -1;
  }

  // Initial conditions
  std::string s;

  double    time;
  unsigned  value;

//  ifs >> time >> value;
//  if (ifs.eof())
//    return 0;

  if (!getline(ifs, s))
    return 0;

  extract_time_and_value(s.c_str(), time, value);

  std::ofstream ofs[2];
  ofs[0].open(argv[2], std::ofstream::binary);
  ofs[1].open(argv[3], std::ofstream::binary);

  uart u[2];

  unsigned  lv[2];
  double    lt[2];

  lt[0] = time;
  lv[0] = (value >> 0) & 1;

  lt[1] = time;
  lv[1] = (value >> 1) & 1;

  for (;;)
  {    
//    ifs >> time >> value;
//    if (ifs.eof())
//      break;

    if (!getline(ifs, s))
      break;

    extract_time_and_value(s.c_str(), time, value);

//    auto pos = s.find('\t');
//    if (pos == std::string::npos)
//      continue;

//    time  = boost::lexical_cast<double>(s.data(), pos);
//    value = boost::lexical_cast<unsigned>(s.data() + pos + 1);

    for (size_t i = 0; i < 2; ++ i)
    {
      unsigned v = value & 1;

      if (v != lv[i])
      {
        const double bitrate = 2e6;
        const double period  = 1 / bitrate;

        double diff_time = time - lt[i];

        int bits = (diff_time / period + 0.5);

        for (int k = 0; k < bits; ++ k)
        {
          u[i].parse_next_bit(lv[i]);
          if (u[i].bnew_)
          {
            if (!u[i].bvalid_)
              std::cerr << "UART" << i << " desync!" << std::endl;
            // std::cout << std::hex << unsigned(u.w_) << std::endl;
            // std::cout << u.w_;
            ofs[i] << u[i].w_;
          }
        }

        lt[i] = time;
        lv[i] = v;
      }

      value >>= 1;
    }
  }

  // std::cout << time << " " << value << " +" << dtime << ", " << bits << "=" << last_value << std::endl;
  return 0;
}
