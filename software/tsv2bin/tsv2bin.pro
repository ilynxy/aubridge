TEMPLATE = app
CONFIG += console c++11 precompile_header
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += static

include(../dir.platform.pri)
include(../boost.pri)

SOURCES += \
  tsv2bin.cpp

