`default_nettype none

module ar429_rx_activity_led
#(
  parameter BLINK_MS = 100
)
(
  input  wire       areset,
  input  wire       clk,
  
  input  wire       ena_1ms,
  
  input  wire       activity,
  
  output wire       led
);

  localparam CW = $clog2(BLINK_MS);

  logic bc_clr;
  logic bc_inc;
  logic [CW-1: 0] bc;
  logic bc_done;

  always_comb
    bc_done = (bc == (BLINK_MS - 1));
    
  always_ff@(posedge areset or posedge clk)
    if (areset)
      bc <= '0;
    else begin
      if (bc_clr)
        bc <= '0;
      else
        bc <= bc + bc_inc;
    end

  logic led_set;
  logic led_q;
  logic led_d;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      led_q <= '0;
    else
      if (led_set)
        led_q <= led_d;
        
  assign led = ~led_q;
  
  enum int unsigned 
  {
    stIDLE,
    stLED_ON,
    stLED_OFF
  } state, next_state;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      state <= stIDLE;
    else
      state <= next_state;
  
  always_comb begin
    next_state = state;
    
    led_set = '0;
    led_d   = 'x;
    
    bc_clr  = '0;
    bc_inc  = '0;
    
    case (state)
    stIDLE: begin
      if (activity) begin
        led_set = '1;
        led_d   = '1;
        
        bc_clr = '1;
        
        next_state = stLED_ON;
      end
    end
    
    stLED_ON: begin
      bc_inc = ena_1ms;
      
      if (bc_done) begin
        led_set = '1;
        led_d   = '0;
        
        bc_clr  = '1;
        next_state = stLED_OFF;
      end
    end
    
    stLED_OFF: begin
      bc_inc = ena_1ms;
      if (bc_done)
        next_state = stIDLE;
    end
    
    endcase
  end
  
endmodule
