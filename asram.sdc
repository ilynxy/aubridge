#
set ramclk_name   $sysclk_name
set ramclk_src    $sysclk_pin
set ramclk_per    $sysclk_per

set clkx2         {clk48000kHz}

#############################################################################
# Write constrains
#############################################################################

# page 6 table Write Cycle values
set tWC     10
set tSCE    7
set tAW     7
set tHA     0
set tSA     0
set tPWE    7
set tSD     5.5
set tHD     0
set tLZWE   3
set tHZWE   5
set tBW     7

# timings according to docs/CY7C1062DV33_38-05477_0I_V.pdf pages 8-9 figures 6,7 (CE or WE controlled) 
set ram_we_reg [get_registers {*CY7C1062*r_we}]
create_generated_clock -add -source $clkx2 -divide_by 2 -name {ram_we_clk} $ram_we_reg
create_generated_clock -add -source $ram_we_reg -name {ram_we_strobe} [get_ports {ASRAM_WE_N}]
set_false_path -to [get_ports {ASRAM_WE_N}]

#set_output_delay -add_delay -clock $ramclk_name -max 12.0  [get_ports {ASRAM_WE_N}]
#set_output_delay -add_delay -clock $ramclk_name -min 0.0   [get_ports {ASRAM_WE_N}]
#set_false_path -fall_from ram_we_clk

set_output_delay -add_delay -clock {ram_we_strobe} -max $tAW  [get_ports {ASRAM_A[*]}]
set_output_delay -add_delay -clock {ram_we_strobe} -min -$tHA [get_ports {ASRAM_A[*]}]

set_output_delay -add_delay -clock {ram_we_strobe} -max $tSD  [get_ports {ASRAM_D[*]}]
set_output_delay -add_delay -clock {ram_we_strobe} -min -$tHD [get_ports {ASRAM_D[*]}]

set_output_delay -add_delay -clock {ram_we_strobe} -max $tSCE [get_ports {ASRAM_CE_N[*]}]
set_output_delay -add_delay -clock {ram_we_strobe} -min 0.0   [get_ports {ASRAM_CE_N[*]}]

set_output_delay -add_delay -clock {ram_we_strobe} -max $tBW  [get_ports {ASRAM_BE_N[*]}]
set_output_delay -add_delay -clock {ram_we_strobe} -min 0.0   [get_ports {ASRAM_BE_N[*]}]

#############################################################################
# Read constrains
#############################################################################

# page 6 table Read Cycle values
set tPOWER  100000
set tRC     10
set tAA     10
set tOHA    3
set tACE    10
set tDOE    5
set tLZOE   1
set tHZOE   5
set tLZCE   3
set tHZCE   5
set tPU     0
set tPD     10
set tDBE    5
set tLZBE   1
set tHZBE   5

# for 48MHz mode
#set tS      2.65

# for 24MHz mode
set tS      10

set_output_delay -add_delay -clock $ramclk_name -max [expr {$tAA + $tS}]  [get_ports {ASRAM_A[*]}]
set_output_delay -add_delay -clock $ramclk_name -min 0.0                  [get_ports {ASRAM_A[*]}]

set_output_delay -add_delay -clock $ramclk_name -max [expr {$tACE + $tS}] [get_ports {ASRAM_CE_N[*]}]
set_output_delay -add_delay -clock $ramclk_name -min 0.0                  [get_ports {ASRAM_CE_N[*]}]

set_output_delay -add_delay -clock $ramclk_name -max [expr {$tDBE + $tS}] [get_ports {ASRAM_BE_N[*]}]
set_output_delay -add_delay -clock $ramclk_name -min 0.0                  [get_ports {ASRAM_BE_N[*]}]

set_output_delay -add_delay -clock $ramclk_name -max [expr {$tDOE + $tS}] [get_ports {ASRAM_OE_N}]
set_output_delay -add_delay -clock $ramclk_name -min 0.0                  [get_ports {ASRAM_OE_N}]

set_input_delay -add_delay -clock $ramclk_name -max [expr {$ramclk_per - $tS}]  [get_ports {ASRAM_D[*]}]
set_input_delay -add_delay -clock $ramclk_name -min $tOHA                       [get_ports {ASRAM_D[*]}]


if {0} {
# timings according to docs/CY7C1062DV33_38-05477_0I_V.pdf pages 7-8 figures 4,5 (OE controlled) 
set ram_oe_reg [get_registers {*CY7C1062*r_oe}]
create_generated_clock -add -source $ramclk_src -name {ram_oe_clk} $ram_oe_reg
create_generated_clock -add -source $ram_oe_reg -name {ram_oe_strobe} [get_ports {ASRAM_OE_N}]
set_false_path -to [get_ports {ASRAM_OE_N}]

set_false_path -from {ram_oe_clk} -to {ram_we_strobe} 

set_input_delay -add_delay -clock {ram_oe_strobe} -max $tDOE  [get_ports {ASRAM_D[*]}]
set_input_delay -add_delay -clock {ram_oe_strobe} -min $tHZOE [get_ports {ASRAM_D[*]}]

set required_regs [get_registers -nowarn {*CY7C1062*r_ce*}]
if {[get_collection_size $required_regs] != 0} {


  for { set i 0 } { $i < 3 } { incr i } {

    set ram_re_clk      "ram_ce${i}_clk"
    set ram_re_reg      [get_registers "*CY7C1062*r_ce\[$i\]"]
    set ram_re_port     [get_ports     "ASRAM_CE_N\[$i\]"]
    set ram_re_strobe   "ram_ce${i}_strobe"
    
    set ram_d_port      [get_ports {ASRAM_D[*]}]
    
    create_generated_clock -add -source $ram_re_reg -name $ram_re_strobe $ram_re_port
    create_generated_clock -add -source $ramclk_src -name $ram_re_clk $ram_re_reg
    set_false_path -from $ram_re_clk -to $ramclk_name
    set_false_path -fall_from $ram_re_clk -to {ram_we_strobe}

    set_input_delay -add_delay -clock $ram_re_strobe -max $tACE  $ram_d_port
    set_input_delay -add_delay -clock $ram_re_strobe -min $tHZCE $ram_d_port
  }

}

set required_regs [get_registers -nowarn {*CY7C1062*r_be*}]
if {[get_collection_size $required_regs] != 0} {

  for { set i 0 } { $i < 4 } { incr i } {

    set ram_re_clk      "ram_be${i}_clk"
    set ram_re_reg      [get_registers "*CY7C1062*r_be\[$i\]"]
    set ram_re_port     [get_ports     "ASRAM_BE_N\[$i\]"]
    set ram_re_strobe   "ram_be${i}_strobe"
    
    set ram_d_port      [get_ports {ASRAM_D[*]}]
    
    create_generated_clock -add -source $ram_re_reg -name $ram_re_strobe $ram_re_port
    create_generated_clock -add -source $ramclk_src -name $ram_re_clk $ram_re_reg

    set_false_path -from $ram_re_clk -to $ramclk_name
    set_false_path -fall_from $ram_re_clk -to {ram_we_strobe}

    set_input_delay -add_delay -clock $ram_re_strobe -max $tDBE  $ram_d_port
    set_input_delay -add_delay -clock $ram_re_strobe -min $tHZBE $ram_d_port
  }

}

set required_regs [get_registers -nowarn {*CY7C1062*r_a*}]
if {[get_collection_size $required_regs] != 0} {

  for { set i 0 } { $i < 19 } { incr i } {

    set ram_re_clk      "ram_a${i}_clk"
    set ram_re_reg      [get_registers "*CY7C1062*r_a\[$i\]"]
    set ram_re_port     [get_ports     "ASRAM_A\[$i\]"]
    set ram_re_strobe   "ram_a${i}_strobe"
    
    set ram_d_port      [get_ports {ASRAM_D[*]}]
    
    create_generated_clock -add -source $ram_re_reg -name $ram_re_strobe $ram_re_port
    create_generated_clock -add -source $ramclk_src -name $ram_re_clk $ram_re_reg

    set_false_path -from $ram_re_clk -to $ramclk_name
    set_false_path -fall_from $ram_re_clk -to {ram_we_strobe}

    set_input_delay -add_delay -clock $ram_re_strobe -max $tAA  $ram_d_port
    set_input_delay -add_delay -clock $ram_re_strobe -min $tOHA $ram_d_port
  }

}

}