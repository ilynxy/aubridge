/*
  Prioritized memory sharing between N interfaces.
  Memory's masters must obey 'busy' signal (exept the first maybe), and
  hold all signals unchanged during 'busy' is active.
*/

module ram_arbitrator
#(
  parameter N = 4
)
(
  input   logic                 areset,
  input   logic                 clk,
  
  // Arbitrator interface
  input   logic [N-1: 0]        ena,
  output  logic [N-1: 0]        busy,
  input   logic [N-1: 0][18: 0] a,
  input   logic [N-1: 0][ 3: 0] be,
  input   logic [N-1: 0][31: 0] d,
  input   logic [N-1: 0]        we,
  output  logic [N-1: 0][31: 0] q,
  output  logic [N-1: 0]        qv,
  
  // Synchronous RAM interface
  output  logic                 ram_ena,
  output  logic [18: 0]         ram_a,
  output  logic [ 3: 0]         ram_be,
  output  logic [31: 0]         ram_d,
  output  logic                 ram_we,
  input   logic [31: 0]         ram_q,
  input   logic                 ram_qv
);
  
  // Priority decoder (pure combinatorial)
  always_comb begin
    ram_ena = '0;
    ram_a   = 'x;
    ram_be  = 'x;
    ram_d   = 'x;
    ram_we  = 'x;
        
    busy    = '0;
    
    for (int i = 0; i < N; ++ i) begin
      if (ena[i]) begin
        for (int k = (i + 1); k < N; ++ k)
          busy[k] = '1;

        ram_ena = '1;
        ram_a   = a[i];
        ram_be  = be[i];
        ram_d   = d[i];
        ram_we  = we[i];

        break;
      end
    end

    q       = { N { ram_q } };
    qv      = { N { ram_qv } };

  end
  

endmodule
