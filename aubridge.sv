`default_nettype none

`include "config.vh"

module aubridge
(
  input   wire          clk48000kHz,

  // ARINC 429 receivers pins (HI-8444PS)
  input   wire [ 7: 0]  AR429_RX_P,
  input   wire [ 7: 0]  AR429_RX_N,
  
  output  wire          AR429_RX_TEST74_P,
  output  wire          AR429_RX_TEST74_N,
  
  output  wire          AR429_RX_TEST30_P,
  output  wire          AR429_RX_TEST30_N,
  
  output  wire          AR429_RX_LED,

  // ARINC 429 transmitters pins (HI-8596PS)
  output  wire [ 3: 0]  AR429_TX_P,
  output  wire [ 3: 0]  AR429_TX_N,
  
  output  wire [ 3: 0]  AR429_TX_SLP,

  // UART pins
  input   wire          UART_RX,
  output  wire          UART_TX,
  
  // Asynchronous static RAM
  output  wire [ 2: 0]  ASRAM_CE_N,
  inout   wire [31: 0]  ASRAM_D,
  output  wire [18: 0]  ASRAM_A,
  output  wire [ 3: 0]  ASRAM_BE_N,
  output  wire          ASRAM_WE_N,
  output  wire          ASRAM_OE_N,
  
  // One-wire temperature sensor DS18B20 pins
  inout   wire          DS18B20_TRAM,
  inout   wire          DS18B20_TAUX
);

  localparam  SYSCLK_FREQ  = 24_000_000;
  localparam  UART_BITRATE =  2_000_000;
  
  localparam  FIRMWARE_VERSION = 16'h01_06; // v 1.05
  // 1'b1 -- reverse bit order of address field, 1'b0 -- don't revesre
`ifdef REVERSE_A429_ADDRESS
  localparam  REVERSE_A429_ADDRESS_FIELD = 1'b1; 
`else
  localparam  REVERSE_A429_ADDRESS_FIELD = 1'b0; 
`endif

  // Force disable test mode for input ARINC 429 lines 7..4
  assign AR429_RX_TEST74_P = '0;
  assign AR429_RX_TEST74_N = '0;
  
  // Force disable test mode for input ARINC 429 lines 3..0
  assign AR429_RX_TEST30_P = '0;
  assign AR429_RX_TEST30_N = '0;

  assign AR429_TX_SLP      = '1;
  
  //assign AR429_RX_LED      = '1;
  
  // global clock and reset

  wire clkx2 = clk48000kHz;
  
  logic sysclk_div = '0;
  always_ff@(posedge clk48000kHz)
    sysclk_div <= ~sysclk_div;

  wire clk = sysclk_div;
  
  logic [3:0] r_reset = '0;
  always_ff@(posedge clk)
    r_reset <= r_reset + !r_reset[3];
  
  wire areset = !r_reset[3];

  //
  logic                 ena_1ms;
  logic                 a429_ena_mes1t;
  logic                 a429_ena_mes4t;
  logic [ 3: 0]         a429_ena_tx2t;
  logic [ 3: 0][ 1: 0]  a429_ssel;

//  always_comb
//    a429_ssel = { 2'b00, 2'b01, 2'b10, 2'b11 };

  a429_serializer_divider#(
    .N       (4),
    .CLKFREQ (SYSCLK_FREQ)
  )
    a429_divider_enas
    (
      .areset     (areset),
      .clk        (clk),
      
      .ena_1ms    (ena_1ms),

      .ena_mes1t  (a429_ena_mes1t),
      .ena_mes4t  (a429_ena_mes4t),

      .ssel       (a429_ssel),
      .ena_tx2t   (a429_ena_tx2t)
    );

  
  localparam    NRAM = 4;
  logic [NRAM-1: 0]        ram_ena;
  logic [NRAM-1: 0]        ram_busy;
  logic [NRAM-1: 0][18: 0] ram_a;
  logic [NRAM-1: 0][ 3: 0] ram_be;
  logic [NRAM-1: 0][31: 0] ram_d;
  logic [NRAM-1: 0]        ram_we;
  logic [NRAM-1: 0][31: 0] ram_q;

  ram_manager#( .N ( NRAM ) )
    ram_share_manager
    (
      .areset     (areset),
      .clk        (clk),
      .clkx2      (clkx2),

      .ena        (ram_ena),
      .busy       (ram_busy),
      .a          (ram_a),
      .be         (ram_be),
      .d          (ram_d),
      .we         (ram_we),
      .q          (ram_q),
      .qv         (), // Assume RAM has 1'cycle latency

      .ASRAM_CE_N (ASRAM_CE_N),
      .ASRAM_D    (ASRAM_D),
      .ASRAM_A    (ASRAM_A),
      .ASRAM_BE_N (ASRAM_BE_N),
      .ASRAM_WE_N (ASRAM_WE_N),
      .ASRAM_OE_N (ASRAM_OE_N)
    );
/*
  always_comb begin
    ram_ena = '0;
    ram_a   = 'x;
    ram_be  = 'x;
    ram_d   = 'x;
    ram_we  = 'x;
  end
*/
  logic           ser_ena;
  logic [ 7: 0]   ser_d;
  logic           ser_load;
  logic           ser_busy;
  logic           ser_tx;


  //
  logic         fa_ws_ena;
  logic         fa_ws_busy;
  logic [31:0]  fa_ws_d;
  
  logic         fa_rs_ena;
  logic         fa_rs_busy;
  logic [31: 0] fa_rs_q;
  logic [ 7: 0] fa_rs_words;

  scc_fifo
    arinc429_fifo
    (
      .areset     (areset),
      .clk        (clk),

      .ram_ena    (ram_ena[1:0]),
      .ram_busy   (ram_busy[1:0]),
      .ram_a      (ram_a[1:0]),
      .ram_be     (ram_be[1:0]),
      .ram_d      (ram_d[1:0]),
      .ram_we     (ram_we[1:0]),
      .ram_q      (ram_q[1:0]),

      .ws_ena     (fa_ws_ena),
      .ws_busy    (fa_ws_busy),
      .ws_d       (fa_ws_d),

      .rs_ena     (fa_rs_ena),
      .rs_busy    (fa_rs_busy),
      .rs_q       (fa_rs_q),
      .rs_words   (fa_rs_words)
    );

  logic [ 7: 0] ar429_rx_activity;
  arinc429_receiver#( .N ( 8 ) )
    a429_to_fifo
    (
      .areset     (areset),
      .clk        (clk),
      
      .ena_mes1t  (a429_ena_mes1t),
      .ena_mes4t  (a429_ena_mes4t),

      .rx_a       (AR429_RX_P),
      .rx_b       (AR429_RX_N),

      .rx_activity  (ar429_rx_activity),
      
      .ws_busy    (fa_ws_busy),
      .ws_ena     (fa_ws_ena),
      .ws_d       (fa_ws_d)
    );

  ar429_rx_activity_led#(
    .BLINK_MS (100)
  )
    ar429_rx_activity_blinker
    (
      .areset     (areset),
      .clk        (clk),
      
      .ena_1ms    (ena_1ms),
      
      .activity   (|ar429_rx_activity),
      
      .led        (AR429_RX_LED)
    );

  //
  logic [15: 0] ram_temperature;

  logic  DS18B20_TRAM_d;
  logic  DS18B20_TRAM_oe;
  logic  DS18B20_TRAM_q;
  assign DS18B20_TRAM   = DS18B20_TRAM_oe ? 1'b0 : 1'bz;
  assign DS18B20_TRAM_q = DS18B20_TRAM;

  onewire_DS18B20#(
    .DIV  (24)
  )
    temperature_sensor_ram
    (
      .areset       (areset),
      .clk          (clk),

      .iob_oe       (DS18B20_TRAM_oe),
      .iob_d        (DS18B20_TRAM_d),
      .iob_q        (DS18B20_TRAM_q),

      .temperature  (ram_temperature)
    );
    
  logic [15: 0] aux_temperature;

  logic  DS18B20_TAUX_d;
  logic  DS18B20_TAUX_oe;
  logic  DS18B20_TAUX_q;
  assign DS18B20_TAUX   = DS18B20_TAUX_oe ? 1'b0 : 1'bz;
  assign DS18B20_TAUX_q = DS18B20_TAUX;

  onewire_DS18B20#(
    .DIV  (24)
  )
    temperature_sensor_aux
    (
      .areset       (areset),
      .clk          (clk),

      .iob_oe       (DS18B20_TAUX_oe),
      .iob_d        (DS18B20_TAUX_d),
      .iob_q        (DS18B20_TAUX_q),

      .temperature  (aux_temperature)
    );

  //
  
  logic [15: 0] r_temp_sensor_ram;
  logic [15: 0] r_temp_sensor_aux;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      r_temp_sensor_ram <= '0;
      r_temp_sensor_aux <= '0;
    end
    else if (ena_1ms) begin
      r_temp_sensor_ram <= ram_temperature;
      r_temp_sensor_aux <= aux_temperature;
    end
  
  logic [ 2: 0][31: 0] einfo;
  logic                einfo_req;
  always_comb begin
    // version info
    einfo[0] = { 8'(REVERSE_A429_ADDRESS_FIELD), FIRMWARE_VERSION, 8'h00 };
    einfo[1] = { 8'h00, r_temp_sensor_ram, 8'h81 }; // RAM temperature sensor
    einfo[2] = { 8'h00, r_temp_sensor_aux, 8'h42 }; // AUX temperature sensor
  end
  
  logic [ 7: 0] writes_count;
  
  frame_packetizer
    frame_generator
    (
      .areset   (areset),
      .clk      (clk),

      .einfo    (einfo),
      .einfo_req(einfo_req),
      
      .commit   (ena_1ms),

      .writes_count (writes_count),
      .rs_words ( (fa_rs_words >> 1) ),

      .rs_ena   (fa_rs_ena),
      .rs_busy  (fa_rs_busy),
      .rs_q     (fa_rs_q),

      .ser_ena  (ser_ena),
      .ser_d    (ser_d),
      .ser_load (ser_load),
      .ser_busy (ser_busy)
    );

  //

  UART_bitrate_divider#(
    .CLKFREQ  (SYSCLK_FREQ),
    .BITRATE  (UART_BITRATE)
  )
    serializer_bitrate_ena
    (
      .areset   (areset),
      .clk      (clk),

      .tx_ena   (ser_ena)
    );
   
  
  logic [ 7: 0] ser_d_;
  logic         ser_load_;
  
  UART_serializer
    serializer
    (
      .areset   (areset),
      .clk      (clk),
  
      .ena      (ser_ena),
  
      .d        (ser_d_),
      .load     (ser_load_),
      .busy     (ser_busy),

      .stx      (ser_tx)
    );

  assign UART_TX = ser_tx;

  localparam APRI = 3;
  
  logic [ 3: 0] chupd_q;
  logic [ 3: 0] chupd_acq;
  
  a429_player#( .N (4) )
    a429_play
    (
      .areset   (areset),
      .clk      (clk),
      
      .ena_1ms  (ena_1ms),

      .a429_ssel(a429_ssel),

      .ram_ena  (ram_ena[APRI]),
      .ram_busy (ram_busy[APRI]),
      .ram_a    (ram_a[APRI]),
      .ram_be   (ram_be[APRI]),
      .ram_d    (ram_d[APRI]),
      .ram_we   (ram_we[APRI]),
      .ram_q    (ram_q[APRI]),

      .tx_ena   (a429_ena_tx2t),
      .tx_a     (AR429_TX_P),
      .tx_b     (AR429_TX_N),
      
      .chupd_q  (chupd_q),
      .chupd_acq(chupd_acq)
    );

  //
  logic ccUART_RX;
  
  ccpipe#(
    .WIDTH(1), 
    .STAGES(3)
  )
    cc_uart_rx
    (
      .areset   (areset),
      .clk      (clk),
      
      .d        (UART_RX),
      .q        (ccUART_RX)
    );

  logic [ 7: 0] deser_q;
  logic         deser_valid;

  UART_deserializer#(
    .CLKFREQ  (SYSCLK_FREQ),
    .BITRATE  (UART_BITRATE)
  )
    uart_deser
    (
      .areset   (areset),
      .clk      (clk),

      .rx       (ccUART_RX),

      .q        (deser_q),
      .valid    (deser_valid)
    );

  logic uf_empty;
  logic uf_valid;
  logic uf_acq;
  logic [ 7: 0] uf_q;

  logic uf_acq_;
  scfifo#(
    .lpm_width      (8),
    .lpm_widthu     (2),
    .lpm_numwords   (4),
    .lpm_showahead  ("ON")
  )
    cmd_fifo
    (
      .aclr     (areset),
      .clock    (clk),
      .wrreq    (deser_valid),
      .data     (deser_q),
      
      .rdreq    (uf_acq_),
      .empty    (uf_empty),
      .q        (uf_q),
      
      .almost_empty (),
      .almost_full  (),
      .full         (),
      .usedw        (),
      .sclr         ()
    );
    
  // UART RX->TX loop test
  always_comb begin
    //ser_d_      = uf_q;
    //ser_load_   = !uf_empty && ser_ena && !ser_busy;
    //uf_acq_     = ser_load_;

    ser_d_ = ser_d;
    ser_load_ = ser_load;
    uf_acq_ = uf_acq;
  end

    
  always_comb
    uf_valid = !uf_empty;

  localparam CPRI = 2;
  uart_command_parser
    command_parser
    (
      .areset       (areset),
      .clk          (clk),

      .deser_q      (uf_q),
      .deser_valid  (uf_valid),
      .deser_acq    (uf_acq),

      .writes_count (writes_count),
      .player_ena   (),

      .ram_ena    (ram_ena[CPRI]),
      .ram_busy   (ram_busy[CPRI]),
      .ram_a      (ram_a[CPRI]),
      .ram_be     (ram_be[CPRI]),
      .ram_d      (ram_d[CPRI]),
      .ram_we     (ram_we[CPRI]),
      .ram_q      (ram_q[CPRI]),
      
      .chupd_q    (chupd_q),
      .chupd_acq  (chupd_acq),
      
      .einfo_req  (einfo_req)
    );

endmodule
