module a429_HI8444PS_adapter
(
  input   logic     areset,
  input   logic     clk,
  
  input   logic     rx_a,
  input   logic     rx_b,
  
  // Start Of Pulse any polarity
  output  logic     sop,
  // End Of Pulse any polarity
  output  logic     eop,
  // Idle state indication
  output  logic     idle,
  // Pulse polarity at EOP event
  output  logic     value
);

  logic   ra;
  logic   rb;

  ccpipe#(
    .WIDTH(2), 
    .STAGES(2)
  )
    cc_rx
    (
      .areset   (areset),
      .clk      (clk),
      
      .d        ({ rx_a, rx_b }),
      .q        ({ ra  , rb   })
    );
/*
  always_ff@(posedge areset or posedge clk)
    if (areset)
      { ra, rb } <= '0;
    else
      { ra, rb } <= { rx_a, rx_b };
*/

  logic is_idle;
  always_comb
    is_idle = !(ra || rb);

  logic prev_is_idle;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      prev_is_idle <= '1;
    else
      prev_is_idle <= is_idle;
  
  logic line_value;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      line_value <= '0;
    else
      line_value <= ra;
  
  always_comb begin
    sop   =  prev_is_idle && !is_idle;
    eop   = !prev_is_idle &&  is_idle;
    idle  = is_idle;
    value = line_value;
  end

endmodule
