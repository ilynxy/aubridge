/*
  Truly asynchronous ram to synchronous ram adapter.
  Write and read requests have 1'cycle latency (due to registered glue logic).
  The 'qv' signal   must be interpret as 'q is valid' event after 'ena'
*/
module asram_CY7C1062_adapter
(
  input   logic         areset,
  input   logic         clk,
  input   logic         clkx2,

  input   logic         ena,
  input   logic [18: 0] a,
  input   logic [ 3: 0] be,
  input   logic [31: 0] d,
  output  logic [31: 0] q,
  output  logic         qv,
  input   logic         we,

  output  wire [ 2: 0]  ASRAM_CE_N,
  inout   wire [31: 0]  ASRAM_D,
  output  wire [18: 0]  ASRAM_A,
  output  wire [ 3: 0]  ASRAM_BE_N,
  output  wire          ASRAM_WE_N,
  output  wire          ASRAM_OE_N
);

  (* preserve *) logic [18: 0] r_a  = '0;
  (* preserve *) logic [ 3: 0] r_be = '0;
  (* preserve *) logic [31: 0] r_d  = '0;
                 logic         r_we_ena = '0;
  (* preserve *) logic         r_we = '0;
  (* preserve *) logic [ 2: 0] r_ce = '0;
  (* preserve *) logic         r_oe = '0;
  
  // logic [31: 0] r_q;

  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      r_a   <= '0;
      r_be  <= '0;
      r_d   <= '0;
      r_we_ena  <= '0;
      r_ce  <= '0;
      r_oe  <= '0;
    end
    else begin
      if (ena) begin
        r_a   <= a;
        r_be  <= be;
        r_d   <= d;
      end
      
      if (ena)
        r_ce  <= '1;
      else
        r_ce  <= '0;

      r_we_ena  <=  we && ena;
      r_oe  <= !we && ena;
    end
  
  always_ff@(posedge areset or posedge clkx2)
    if (areset)
      r_we <= '0;
    else
      if (r_we_ena)
        r_we <= ~r_we;
  
  assign ASRAM_CE_N = ~r_ce;
  assign ASRAM_A    =  r_a;
  assign ASRAM_BE_N = ~r_be;
  assign ASRAM_WE_N = ~r_we;
  assign ASRAM_OE_N = ~r_oe;
  assign ASRAM_D    =  r_we_ena ? r_d : 32'bz;

  always_comb begin
    q   = ASRAM_D;
    qv  = r_ce[0];
  end

endmodule
