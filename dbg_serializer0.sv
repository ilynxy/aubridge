module dbg_serializer0
(
  input   logic         areset,
  input   logic         clk,
  
  input   logic         ser_ena,
  output  logic [ 7: 0] ser_d,
  output  logic         ser_load,
  input   logic         ser_busy
);

  // Debug serializer simple counter
  logic [ 7: 0] cnt;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      cnt <= '0;
    else
      if (ser_ena)
        cnt <= cnt + ser_load;
  
  always_comb begin
    ser_d     = cnt;
    ser_load  = !ser_busy;
  end

endmodule
