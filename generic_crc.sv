module generic_crc
#(
  parameter CRC_WIDTH     = 16,
  parameter CRC_POLYNOME  = 16'h1021,
  parameter CRC_INITIAL   = 16'hFFFF,
  parameter CRC_XOR       = 16'h0000,

  parameter DATA_WIDTH    = 8,

  parameter DATA_REFLECT  = 0,
  parameter CRC_REFLECT   = 0
)
(
  input  logic                   reset_n,
  input  logic                   clk,
  
  input  logic                   init,
  input  logic [DATA_WIDTH-1:0]  d,
  input  logic                   ena,
  
  output logic [CRC_WIDTH-1:0]   q
);

  typedef logic [DATA_WIDTH-1:0] data_t;
  typedef logic [CRC_WIDTH-1:0]  crc_t;
    
  crc_t   r_crc;
  always_ff@(negedge reset_n or posedge clk)
    if (!reset_n)
      r_crc <= CRC_INITIAL;
    else
      if (init)
        r_crc <= CRC_INITIAL;
      else 
        if (ena)
          r_crc <= crc_next(d, r_crc);

  assign q = r_crc ^ CRC_XOR;
  
  localparam RPOLY = reflect_crc(CRC_POLYNOME);
  // crc_t RPOLY = reflect_crc(CRC_POLYNOME);
  
  function crc_t crc_next(input data_t d, input crc_t c);
  begin
    crc_t v;
    v = c;    
    for (int i = 0; i < DATA_WIDTH; i ++) begin
      logic b;

      if (DATA_REFLECT != 0) begin
        b = d[0];
        d = d >> 1;
      end
      else begin
        b = d[DATA_WIDTH-1];
        d = d << 1;
      end

      if (CRC_REFLECT != 0) begin
        b = b ^ v[0];
        v = (v >> 1) ^ (RPOLY & { CRC_WIDTH { b} });
      end
      else begin
        b = b ^ v[CRC_WIDTH-1];
        v = (v << 1) ^ (CRC_POLYNOME & { CRC_WIDTH { b } });
      end
    end

    crc_next = v;
  end
  endfunction
  
  function crc_t  reflect_crc(input crc_t c);
  begin
    for (int i = 0; i < CRC_WIDTH; i ++)
      reflect_crc[i] = c[CRC_WIDTH-1 - i];
  end
  endfunction

endmodule
