module UART_serializer
(
  input   logic         areset,
  input   logic         clk,
  
  input   logic         ena,
  
  input   logic [ 7: 0] d,
  input   logic         load,
  output  logic         busy,

  output  logic         stx
);
  
  logic [ 3: 0] bcnt;
  logic         r_busy;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      r_busy  <= '0;
      bcnt    <= '1;
    end
    else
      if (ena) begin
        if (load) begin
          r_busy  <= '1;
          bcnt    <= '0;
        end
        else begin
          r_busy <= !bcnt[3]; // bcnt == '8
          bcnt <= bcnt + busy;
        end
      end
      
  always_comb
    busy = r_busy;

  logic [ 8: 0] r_sh;

  always_ff@(posedge areset or posedge clk)
    if (areset)
      r_sh <= '1;
    else
      if (ena) begin
        if (load)
          r_sh <= { d, 1'b0 };
        else
          r_sh <= { 1'b1, r_sh[ 8: 1] };
      end

  always_comb
    stx <= r_sh[0];

endmodule
