module a429_player
#(
  parameter N = 4
)
(
  input   logic                 areset,
  input   logic                 clk,
  
  input   logic                 ena_1ms,
  
  output  logic [N-1: 0][ 1: 0] a429_ssel,
  
  output  logic                 ram_ena,
  input   logic                 ram_busy,
  output  logic         [18: 0] ram_a,
  output  logic         [ 3: 0] ram_be,
  output  logic         [31: 0] ram_d,
  output  logic                 ram_we,
  input   logic         [31: 0] ram_q,
  
  input   logic         [N-1:0] tx_ena,
  output  logic         [N-1:0] tx_a,
  output  logic         [N-1:0] tx_b,
  
  input   logic         [ 3: 0] chupd_q,
  output  logic         [ 3: 0] chupd_acq
);

  logic [N-1: 0]        ser_busy;
  logic [N-1: 0]        ser_load;
  logic [N-1: 0][31: 0] ser_d;

  genvar i;
  generate
    for (i = 0; i < N; ++ i) begin : stx
      a429_serializer
      aserializer
      (
        .areset     (areset),
        .clk        (clk),

        .ena        (tx_ena[i]),
        
        .d          (ser_d[i]),
        .load       (ser_load[i]),
        .busy       (ser_busy[i]),

        .tx_a       (tx_a[i]),
        .tx_b       (tx_b[i])
      );
    end : stx
  endgenerate

  a429_player_fsm#( .N (N) )
    player_fsm
    (
      .areset     (areset),
      .clk        (clk),
      
      .ena_1ms    (ena_1ms),
      
      .ram_ena    (ram_ena),
      .ram_busy   (ram_busy),
      .ram_a      (ram_a),
      .ram_be     (ram_be),
      .ram_d      (ram_d),
      .ram_we     (ram_we),
      .ram_q      (ram_q),
      
      .ser_busy   (ser_busy),
      .ser_load   (ser_load),
      .ser_d      (ser_d),
      
      .a429_ssel  (a429_ssel),

      .chupd_q    (chupd_q),
      .chupd_acq  (chupd_acq)
    );

endmodule

