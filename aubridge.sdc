create_clock -period "48.000MHz" -name {clk48000kHz} [get_ports {clk48000kHz}]

create_generated_clock -divide_by 2 -name {clk24000kHz} -source [get_ports {clk48000kHz}] [get_registers {*sysclk_div*}]

set sysclk_name {clk24000kHz}
set sysclk_pin  [get_registers {*sysclk_div*}]
set sysclk_per  [get_clock_info -period $sysclk_name]


set_false_path -from [get_ports {AR429_RX_P[*] AR429_RX_N[*]}]
set_false_path -to   [get_ports {AR429_RX_TEST*_P AR429_RX_TEST*_N}]

set_false_path -to   [get_ports {AR429_TX_P[*] AR429_TX_N[*]}]
set_false_path -to   [get_ports {AR429_TX_SLP[*]}]


set_false_path -from [get_ports {UART_RX}]
set_false_path -to   [get_ports {UART_TX}]

set_false_path -to    [get_ports {DS18B20_*}]
set_false_path -from  [get_ports {DS18B20_*}]

set_false_path -to    [get_ports {AR429_RX_LED}]
