module a429_queuer
#(
  parameter N = 8
)
(
  input   logic                 areset,
  input   logic                 clk,
  
  input   logic [N-1: 0][31: 0] a429_q,
  input   logic [N-1: 0]        a429_qv,
  output  logic [N-1: 0]        a429_cv,
  
  input   logic                 ws_busy,
  output  logic                 ws_ena,
  output  logic         [31: 0] ws_d
);
  
  localparam IW = $clog2(N);
  
  logic [IW: 0]  lucnt;
  logic [ 1: 0]  lucnt_add;

  always_ff@(posedge areset or posedge clk)
    if (areset)
      lucnt <= '0;
    else
      lucnt <= lucnt + lucnt_add;

  logic [IW-1: 0] index;
  logic           lo_part;
  logic           hi_part;
      
  always_comb begin
    index   = lucnt[IW:1];
    lo_part = !lucnt[0];
    hi_part =  lucnt[0];
  end
  
  always_comb begin
    ws_ena  = '0;
    ws_d    = 'x;
    
    a429_cv   = '0;

    lucnt_add = lo_part ? 2'd2 : 2'd0;

    if (lo_part && a429_qv[index]) begin
      ws_ena          = '1;
      ws_d[ 7: 0]     = index;
      lucnt_add       = (!ws_busy) ? 2'd1 : 2'd0;
    end
    
    if (hi_part && a429_qv[index]) begin
      ws_ena          = '1;
      ws_d[31: 0]     = a429_q[index];
      lucnt_add       = (!ws_busy) ? 2'd1 : 2'd0;
      a429_cv[index]  = !ws_busy;
    end

  end

endmodule
