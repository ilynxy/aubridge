module a429_player_fsm
#(
  parameter N = 4
)
(
  input   logic                 areset,
  input   logic                 clk,
  
  input   logic                 ena_1ms,
  
  output  logic                 ram_ena,
  input   logic                 ram_busy,
  output  logic         [18: 0] ram_a,
  output  logic         [ 3: 0] ram_be,
  output  logic         [31: 0] ram_d,
  output  logic                 ram_we,
  input   logic         [31: 0] ram_q,
  
  input   logic [N-1: 0]        ser_busy,
  output  logic [N-1: 0]        ser_load,
  output  logic [N-1: 0][31: 0] ser_d,
  
  output  logic [N-1: 0][ 1: 0] a429_ssel,

  input   logic         [ 3: 0] chupd_q,
  output  logic         [ 3: 0] chupd_acq
);
  
  localparam VAR_OFFS = 19'h02000;
  localparam IMG_OFFS = 19'h10000;

  typedef enum logic [ 3: 0]
  {
    stLOAD_OFFSET,
    stLATCH_OFFSET,
    stLOAD_LENGTH,
    stLATCH_LENGTH,
    stCHECK_LENGTH,
    stLOAD_DATA,
    stLATCH_DATA,
    stSEND_DATA,
    
    stNEXT_CHANNEL
  } state_t;

  state_t state = stLOAD_OFFSET;
  state_t next_state;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      state <= stLOAD_OFFSET;
    else
      state <= next_state;

  logic [ 1: 0] idx = '0;
  logic next_channel;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      idx <= '0;
    else
      if (next_channel)
        idx <= idx + 1'b1;
        
  logic [18: 0] offset_addr;
  logic [18: 0] length_addr;
  always_comb begin
    offset_addr = VAR_OFFS + idx * 19'd2 + 19'd0;
    length_addr = VAR_OFFS + idx * 19'd2 + 19'd1;
  end
  
  logic [N-1: 0][ 1: 0] ssel = '0;
  
  logic [15: 0] offset = '0;
  logic         latch_offset;
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      offset  <= '0;
      ssel    <= '0;
    end
    else begin
      if (latch_offset) begin
        offset    <= ram_q[15: 0];
        ssel[idx] <= ram_q[25:24];
      end
    end
  
  always_comb
    a429_ssel = ssel;

  typedef logic [ 9: 0] pause_t;
  
  logic [15: 0] length;
  pause_t pause_val;

  logic         latch_length;
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      length <= '0;
      pause_val <= '0;
    end
    else
      if (latch_length) begin
        length <= ram_q[15: 0];
        pause_val <= pause_t'(ram_q[31:16]);
      end

  logic ena_pause;
  logic dec_pause;
  logic set_pause;
    
  pause_t [N-1: 0] pause = '0;

  logic cpause_is_0;
  logic cpause_is_inf;
  always_comb begin
    cpause_is_0   = (pause[idx] == '0);
    cpause_is_inf = (pause[idx] == '1);
  end

  always_ff@(posedge areset or posedge clk)
    if (areset)
      pause <= '0;
    else
      if (ena_pause) begin
        for (int i = 0; i < N; ++ i) begin
          if (i == idx && set_pause)
            pause[i] <= pause_val;
          else begin
            if ( (pause[i] != '0) && (pause[i] != '1) && dec_pause )
              pause[i] <= pause[i] - 1'b1;
          end
        end
      end

  logic ena_ptr;
  logic clr_ptr;
  logic [N-1: 0][15: 0] ptr = '0;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      ptr <= '0;
    else
      if (ena_ptr) begin
        if (clr_ptr)
          ptr[idx] <= '0;
        else
          ptr[idx] <= ptr[idx] + 1'b1;
      end
  logic [15: 0] cptr;
  always_comb
    cptr = ptr[idx];
    
  logic [18: 0] caddr;
  always_comb
    caddr = IMG_OFFS + 16'(offset + cptr);
    
  
  logic [31: 0] data = '0;
  logic latch_data;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      data <= '0;
    else
      if (latch_data)
        data <= ram_q;
        
  logic         cser_busy;
  logic         cser_load;
  logic [31: 0] cser_d;
  
  always_comb begin
    cser_busy     = ser_busy[idx];
    
    ser_d         = 'x;
    ser_d[idx]    = cser_d;
    
    ser_load      = '0;
    ser_load[idx] = cser_load;
  end
  
  logic cupd_evt;
  logic cupd_acq;
  
  always_comb begin
    cupd_evt        = chupd_q[idx];
    
    chupd_acq       = '0;
    chupd_acq[idx]  = cupd_acq;
  end

  always_comb begin
    next_state = state;
    
    ram_ena = '0;
    ram_a   = 'x;
    ram_be  = '1;
    ram_d   = 'x;
    ram_we  = '0;
    
    next_channel  = '0;
    
    latch_offset  = '0;
    latch_length  = '0;
    latch_data    = '0;
    
    ena_ptr       = '0;
    clr_ptr       = '0;
    
    ena_pause     = '1;
    dec_pause     =  ena_1ms;
    set_pause     = '0;
    
    cser_d        = 'x;
    cser_load     = '0;
    
    cupd_acq      = '0;

    case (state)
    stLOAD_OFFSET: begin
      ram_ena = '1;
      ram_a   = offset_addr;

      if (!ram_busy)
        next_state = stLATCH_OFFSET;
    end
    
    stLATCH_OFFSET: begin
      next_state = stLOAD_LENGTH;
      latch_offset  = '1;
    end
      
    stLOAD_LENGTH: begin
      ram_ena = '1;
      ram_a   = length_addr;
      if (!ram_busy)
        next_state = stLATCH_LENGTH;
    end

    stLATCH_LENGTH: begin
      next_state = stCHECK_LENGTH;
      latch_length  = '1;
    end

    stCHECK_LENGTH: begin
    
      cupd_acq = cupd_evt;
      
      if (length <= cptr) begin

        if (cpause_is_0) begin
          ena_ptr    = '1;
          clr_ptr    = '1;
        end
        
        if (cpause_is_inf) begin
          if (cupd_evt) begin
            cupd_acq = '1;
            
            ena_ptr  = '1;
            clr_ptr  = '1;
          end
        end

        next_state = stNEXT_CHANNEL;
      end
      else begin
        next_state = stLOAD_DATA;

        set_pause  = '1;
      end
    end
    
    stLOAD_DATA: begin
      ram_ena = '1;
      ram_a   = caddr;
      if (!ram_busy)
        next_state = stLATCH_DATA;
    end
    
    stLATCH_DATA: begin
      latch_data = '1;
      next_state = stSEND_DATA;
    end
    
    stSEND_DATA: begin
      if (!cser_busy) begin
        cser_load = '1;
        cser_d    = data;

        ena_ptr   = '1;
      end
      
      next_state = stNEXT_CHANNEL;
    end
    
    stNEXT_CHANNEL: begin
      next_channel = '1;
      next_state   = stLOAD_OFFSET;
    end

    endcase
  end
  
  
endmodule

