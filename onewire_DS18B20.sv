module onewire_DS18B20
#(
  parameter DIV = 66
)
(
  input  logic        areset,
  input  logic        clk,

  output logic        iob_oe,
  output logic        iob_d,
  input  logic        iob_q,

  output logic [15:0] temperature
);

  logic ena;
  onewire_ena_1us#(
    .DIV    (DIV)
  )
    ena_1us_divider
    (
      .areset   (areset),
      .clk      (clk),
      
      .ena_1us  (ena)
    );

  logic       ow_req_reset;
  logic       ow_req_write;
  logic       ow_req_done;
  logic [7:0] ow_d;
  logic [7:0] ow_q;
  onewire_brw
    ow_transact
    (
      .areset   (areset),
      .clk      (clk),
      
      .ena      (ena),

      .process  (ow_req_reset || ow_req_write),
      .mode     (ow_req_reset),

      .done     (ow_req_done),

      .d        (ow_d),
      .q        (ow_q),

      .iob_oe   (iob_oe),
      .iob_d    (iob_d),
      .iob_q    (iob_q)
    );

  // RESET pulse/presence
  // WRITE 0xCC, "Skip ROM"
  // WRITE 0x44, "Convert T"
  // WAIT 750 ms
  // WRITE 0xFF, Check returned byte is !0x00 (still conversing)
  // RESET pulse/presence check
  // WRITE 0xCC, "Skip ROM"
  // WRITE 0xBE, "Read scratchpad"
  // WRITE 0xFF x 9 times, return bytes
  //      Byte 0 Temperature LSB
  //      Byte 1 Temperature MSB
  //      Byte 2 TH Register or User Byte 1
  //      Byte 3 TL Register or User Byte 2
  //      Byte 4 Configuration Register
  //      Byte 5 Reserved (FFh)
  //      Byte 6 Reserved
  //      Byte 7 Reserved (10h)
  //      Byte 8 CRC*
  
  enum logic [3:0]
  {
    stIDLE,
    stRESET_0,
    stSKIP_ROM_0,
    stCONVERT_T,
    stWAIT,
    stCHECK_READY,
    stRESET_1,
    stSKIP_ROM_1,
    stREAD_SCRATCHPAD,
    stGET_DATA_LSB,
    stGET_DATA_MSB
  } state, next_state;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      state <= stIDLE;
    else
      if (ena)
        state <= next_state;

  logic       start = '1;
  logic wcw_done;

  always_comb begin
    
    ow_req_reset = '0;
    ow_req_write = '0;
    ow_d         = 'x;

    next_state = state;

    case (state)
      stIDLE:
        if (start)
          next_state = stRESET_0;
          
      stRESET_0: 
      begin
        ow_req_reset = '1;
        if (ow_req_done)
          next_state = ow_q[0] ? stIDLE : stSKIP_ROM_0; // detect presence
      end
      
      stSKIP_ROM_0:
      begin
        ow_req_write = '1;
        ow_d         = 8'hCC;
        if (ow_req_done)
          next_state = stCONVERT_T;
      end

      stCONVERT_T:
      begin
        ow_req_write = '1;
        ow_d         = 8'h44;
        if (ow_req_done)
          next_state = stWAIT;
      end
      
      stWAIT:
      begin
        if (wcw_done)
          next_state = stCHECK_READY;
      end

      stCHECK_READY:
      begin
        ow_req_write = '1;
        ow_d         = 8'hFF;
        if (ow_req_done)
          next_state = (ow_q == 0) ? stIDLE : stRESET_1;
      end
      
      stRESET_1:
      begin
        ow_req_reset = '1;
        if (ow_req_done)
          next_state = ow_q[0] ? stIDLE : stSKIP_ROM_1;
      end
      
      stSKIP_ROM_1:
      begin
        ow_req_write = '1;
        ow_d         = 8'hCC;
        if (ow_req_done)
          next_state = stREAD_SCRATCHPAD;
      end
      
      stREAD_SCRATCHPAD:
      begin
        ow_req_write = '1;
        ow_d         = 8'hBE;
        if (ow_req_done)
          next_state = stGET_DATA_LSB;
      end
      
      stGET_DATA_LSB:
      begin
        ow_req_write = '1;
        ow_d         = 8'hFF;
        if (ow_req_done)
          next_state = stGET_DATA_MSB;
      end
      
      stGET_DATA_MSB:
      begin
        ow_req_write = '1;
        ow_d         = 8'hFF;
        if (ow_req_done)
          next_state = stIDLE;
      end
    endcase
  end
  
  localparam TCONV = 750_000; // tCONV = 750 ms = 750 000 us
  localparam WCW = $clog2(TCONV);
  typedef logic [WCW-1:0] wcw_t;
  
  wcw_t wcw;
  
  assign wcw_done = (wcw == '0);
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      wcw <= '0;
    else
      if (ena) begin
        if ( (state == stCONVERT_T) && ow_req_done) begin
          wcw <= wcw_t'(TCONV);
        end
        else begin
          if (state == stWAIT)
            wcw <= wcw - 1'b1;
        end
      end

  logic [15:0] r_temperature;
  logic [7:0]  r_lsb;
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      r_temperature <= 16'h8000;
      r_lsb         <= '0;
    end
    else
      if (ena) begin
        if (state == stGET_DATA_LSB && ow_req_done)
          r_lsb <= ow_q;

        if (state == stGET_DATA_MSB && ow_req_done)
          r_temperature <= { ow_q, r_lsb };
      end

  assign temperature = r_temperature;

endmodule

module onewire_brw
(
  input  logic            areset,
  input  logic            clk,
  
  input  logic            ena,
  
  input  logic            process,
  input  logic            mode,

  output logic            done,

  input  logic [ 7: 0]    d,
  output logic [ 7: 0]    q,
  
  output logic            iob_oe,
  output logic            iob_d,
  input  logic            iob_q
);

  localparam tSLOT    = 60;   // slot time
  localparam tREC     = 1;    // recovery time
  localparam tLOW0    = 60;   // write 0 time
  localparam tLOW1    = 1;    // write 1 time
  localparam tRDV     = 15;   // read data valid time
  
  localparam tRSTL    = 480;  // reset low time
  localparam tRSTH    = 480;  // reset high time 

  localparam tPDHmin  = 15;   // presence detect high time
  localparam tPDHmax  = 60;   // presence detect high time
  
  localparam tPDLmin  = 60;   // presence detect low  time
  localparam tPDLmax  = 240;  // presence detect low  time
  
  localparam tRESET   = tRSTH + tRSTL;
  localparam tTRANS   = tSLOT + tREC;
  
  localparam TCW = $clog2(tRESET + 1);
  
  typedef logic [TCW-1: 0] tcnt_t;
  tcnt_t total_time;
  tcnt_t drive_time;
  tcnt_t sample_time;
  
  typedef logic [2:0] bcnt_t;
  bcnt_t total_bits;

  bcnt_t bcnt;
  
  logic is_reset;
  logic is_write_1;
  
  always_comb begin
    is_reset    = mode;
    is_write_1  = d[bcnt];
  end

  always_comb begin
    if (is_reset) begin
      total_time  = tcnt_t'(tRESET);
      drive_time  = tcnt_t'(tRSTL);
      sample_time = tcnt_t'(tRSTL + tPDHmax + tPDHmin / 2);
      total_bits  = bcnt_t'(1 - 1);
    end
    else begin
      total_time  = tcnt_t'(tTRANS);
      drive_time  = is_write_1 ? tcnt_t'(tLOW1) : tcnt_t'(tLOW0);
      sample_time = tcnt_t'(tRDV - tREC);
      total_bits  = bcnt_t'(8 - 1);
    end
  end

  logic one_bit_done;
  logic q_sampled;
  logic q_bit;

  onewire_transact#(
    .TCW  (TCW)
  )
    ow_bit_transact
    (
      .areset       (areset),
      .clk          (clk),

      .ena          (ena),

      .process      (process),
      .done         (one_bit_done),
      .q_sampled    (q_sampled),
      .q            (q_bit),

      .total_time   (total_time),
      .drive_time   (drive_time),
      .sample_time  (sample_time),

      .iob_oe       (iob_oe),
      .iob_d        (iob_d),
      .iob_q        (iob_q)
    );

  logic all_bits_done;
  always_comb
    all_bits_done = (bcnt == total_bits) && one_bit_done;

  logic [7:0] q_out;
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      bcnt <= '0;
      q_out <= '0;
    end
    else
      if (ena) begin
        if (process) begin
          if (one_bit_done)
            bcnt  <= all_bits_done ? bcnt_t'(0) : bcnt_t'(bcnt + 1'b1);
            
          if (q_sampled)
            q_out[bcnt] <= q_bit;

        end
      end

  assign done = all_bits_done;
  assign q    = q_out;
endmodule

module onewire_transact
#(
  parameter   TCW = 10 // Time counter width
)
(
  input  logic            areset,
  input  logic            clk,
  
  input  logic            ena,
  
  input  logic            process,
  output logic            done,
  output logic            q_sampled,
  output logic            q,
  
  input  logic [TCW-1: 0] total_time,   // Total slot duration
  input  logic [TCW-1: 0] drive_time,   // Drive duration ( <= Total slot duration )
  input  logic [TCW-1: 0] sample_time,  // Sample point   ( <= Total slot duration )
  
  output logic            iob_oe,
  output logic            iob_d,
  input  logic            iob_q
);
  
  typedef logic [TCW-1: 0] tcnt_t;
  tcnt_t cnt;

  assign done = (cnt == total_time);

  always_ff@(posedge areset or posedge clk)
    if (areset)
      cnt <= '0;
    else
      if (ena) begin
        if (process)
          cnt <= done ? tcnt_t'(0) : tcnt_t'(cnt + 1'b1);
      end

  logic start_equ;
  logic drive_equ;
  logic sample_equ;
  
  always_comb begin
    start_equ   = (cnt == '0);
    drive_equ   = (cnt == drive_time);
    sample_equ  = (cnt == sample_time);
  end

  logic   r_oe;
  logic   r_d = '0;
  logic   r_q;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      r_oe <= '0;
    else
      if (ena) begin
        if (process) begin
          if (start_equ)
            r_oe <= '1;
          else if (drive_equ)
            r_oe <= '0;
        end
      end
      
  logic sampled;
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      r_q <= '0;
      sampled <= '0;
    end
    else
      if (ena) begin
        if (sample_equ)
          r_q <= iob_q;
        sampled <= sample_equ;
      end
          
  always_comb begin
    iob_oe = r_oe;
    iob_d  = r_d;

    q         = r_q;
    q_sampled = sampled;
  end

endmodule


module onewire_ena_1us
#(
  parameter DIV = 66
)
(
  input  logic      areset,
  input  logic      clk,
  
  output logic      ena_1us
);

  localparam  CW  = $clog2(DIV);
  
  typedef logic [CW-1:0] cnt_t;
  
  cnt_t cnt;
  cnt_t cnt_next;
  logic cout;
  
  always_comb begin
    cnt_next = cnt + 1'b1;
    cout     = (cnt == (DIV-1));
  end
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      cnt <= '0;
    else
      if (cout)
        cnt <= '0;
      else
        cnt <= cnt_next;
  
  always_comb
    ena_1us = cout;

endmodule
