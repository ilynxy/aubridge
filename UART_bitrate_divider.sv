module UART_bitrate_divider
#(
  parameter CLKFREQ = 24_000_000,
  parameter BITRATE =  2_000_000
)
(
  input   logic         areset,
  input   logic         clk,

  output  logic         tx_ena
);

  function integer cdivr(input integer a, input integer b);
    cdivr = (a + (b / 2)) / b;
  endfunction
  
  localparam DIV_VAL    = cdivr(CLKFREQ, BITRATE);
  localparam DIV_WIDTH  = $clog2(DIV_VAL);

  logic [DIV_WIDTH-1: 0] dcnt;
  
  logic wrap;
  always_comb
    wrap = (dcnt == (DIV_VAL - 1));
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      dcnt <= '0;
    else
      if (wrap)
        dcnt <= '0;
      else
        dcnt <= dcnt + 1'b1;

  always_comb
    tx_ena = wrap;

endmodule
