//`default_nettype none

`include "config.vh"

module a429_deserializer
(
  input   logic           areset,
  input   logic           clk,
  
  input   logic           ena_mes1t,
  input   logic           ena_mes4t,
  
  input   logic           rx_a,
  input   logic           rx_b,
  
  output  logic [31: 0]   q,
  output  logic           valid,
  input   logic           flush
);

  logic sop;
  logic eop;
  logic idle;
  logic value;

  a429_HI8444PS_adapter
    line_adapter
    (
      .areset   (areset),
      .clk      (clk),

      .rx_a     (rx_a),
      .rx_b     (rx_b),

      .sop      (sop),
      .eop      (eop),
      .idle     (idle),
      .value    (value)
  );

  // Shift bit in register at pulse end
  logic [31: 0] aword;
  a429_shiftreg
    shiftreg
    (
      .areset   (areset),
      .clk      (clk),
  
      .ena      (eop),
      .d        (value),
      .q        (aword)
    );

  logic [ 4: 0] bc;
  logic         clear_bc;

  a429_bitcounter
    bitcounter
    (
      .areset   (areset),
      .clk      (clk),
      .ena      (1'b1),

      .sclr     (clear_bc),
      .scnt     (eop),

      .q        (bc)
    );

  logic fourT_detected; // really something between 2T and 3T, but it is enough anyway

  a429_wordstop_detector#( .W ( 8 ) )
    stop_detector
    (
      .areset       (areset),
      .clk          (clk),

      .ena1T        (ena_mes1t),
      .ena4T        (ena_mes4t),

      .sop          (sop),
      .idle         (idle),

      .detected     (fourT_detected)
    );

  logic qv;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      qv <= '0;
    else
      qv <= ( qv && !flush && !(qv && eop) ) || (bc == 5'd31 && eop);

  always_comb
    clear_bc = fourT_detected;

  function logic [23: 0] rbo24(input logic [23: 0] x);
    for (int i = 0; i < 24; i ++)
      rbo24[i] = x[24 - i - 1];
  endfunction

  function logic [ 7: 0] rbo8(input logic [ 7: 0] x);
    for (int i = 0; i < 8; i ++)
      rbo8[i] = x[8 - i - 1];  
  endfunction

  always_comb begin
`ifdef REVERSE_A429_ADDRESS
    q     = { rbo24(aword[23: 0]), aword[31:24] }; // reverse bit order in data part
`else
    q     = { rbo24(aword[23: 0]), rbo8(aword[31:24]) }; // reverse bit order in data part
`endif
    valid = qv;
  end

endmodule

module a429_wordstop_detector
#(
  parameter W = 8
)
(
  input   logic         areset,
  input   logic         clk,

  input   logic         ena1T,
  input   logic         ena4T,
  
  input   logic         sop,
  input   logic         idle,
  
  output  logic         detected
);

  logic [W-1: 0] ipw;

  logic [W-1: 0] delta;
  
  always_comb begin
    
    delta = 0;
    
    if (idle && ena4T)
      delta = '1;
    if (!idle && ena1T)
      delta =  1;

  end
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      ipw <= '0;
    else begin
      if (sop)
        ipw <= '0;
      else begin
          ipw <= ipw + delta;
      end
    end

  always_comb
    detected = (ipw == '1);

endmodule

module a429_bitcounter
(
  input   logic         areset,
  input   logic         clk,
  input   logic         ena,
  
  input   logic         sclr,
  input   logic         scnt,

  output  logic [4: 0]  q
);
  
  logic [ 4: 0] counter;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      counter <= '0;
    else
      if (ena) begin
        if (sclr)
          counter <= '0;
        else
          counter <= counter + scnt;
      end

  always_comb
    q = counter;
endmodule

module a429_shiftreg
(
  input   logic         areset,
  input   logic         clk,
  
  input   logic         ena,
  input   logic         d,
  output  logic [31: 0] q
);

  logic [31: 0] shiftreg;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      shiftreg <= '0;
    else
      if (ena)
        shiftreg <= { shiftreg[30: 0], d };

  always_comb
    q = shiftreg;

endmodule
