# UART serdes testbench

set library_name {uart_tx_tb}

set file_list {
  {clockgen_tb.sv}
  {uart_tx_tb.sv}
  {../UART_bitrate_divider.sv}
  {../UART_serializer.sv}
  {../UART_deserializer.sv}
  {../dbg_serializer0.sv}
}

set top_level {uart_tx_tb}

proc tbcompile {} {
  uplevel #0 source compile_tb.tcl
}

proc tbupdate {} {
  set last_compile_time 0
  tbcompile
}

proc tbrun {} {
  restart -force
  run -all
}

tbupdate

eval vsim -L altera_mf_ver -voptargs="+acc" $top_level

# tbrun
