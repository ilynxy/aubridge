`timescale 1 ns / 1 ps

module aubridge_tb
(
);

  initial begin
    #100000000;
    $stop;
  end

  localparam SYSCLK_FREQ = 48_000_000;

  logic areset;
  logic clk;

  clockgen_tb#(
    .FREQ        (SYSCLK_FREQ),
    .RESET_DELAY (40.0),
    .JITTER      (0.000)
  )
    sysclk_gen
    (
      .areset (areset),
      .clk    (clk)
    );
/*
  logic [ 3: 0]         a429_ena_tx2t;
  logic [ 3: 0][ 1: 0]  a429_ssel;
  always_comb
    a429_ssel = { 2'b00, 2'b01, 2'b10, 2'b11 };

  a429_serializer_divider#(
    .N       (4),
    .CLKFREQ (SYSCLK_FREQ)
  )
    a429_divider_enas
    (
      .areset     (areset),
      .clk        (clk),
      
      .ena_1ms    (),

      .ena_mes1t  (),
      .ena_mes4t  (),

      .ssel       (a429_ssel),
      .ena_tx2t   (a429_ena_tx2t)
    );

  logic         ser_ena;
  logic [31: 0] ser_d;
  logic         ser_busy;
  logic         ser_load;
  logic         ser_tx_a;
  logic         ser_tx_b;
    
  always_comb begin
    ser_ena   = a429_ena_tx2t[3];
    ser_load  = !ser_busy;
    ser_d     = 32'h12345678;
  end

  a429_serializer
    serializer
    (
      .areset (areset),
      .clk    (clk),

      .ena    (ser_ena),

      .d      (ser_d),

      .load   (ser_load),
      .busy   (ser_busy),

      .tx_a   (ser_tx_a),
      .tx_b   (ser_tx_b)
    );
*/

  logic           ser_ena;
  logic [ 7: 0]   ser_d;
  logic           ser_load = '0;
  logic           ser_busy;
  logic           ser_tx;

  localparam UART_BITRATE = 2_000_000;
  UART_bitrate_divider#(
    .CLKFREQ  (SYSCLK_FREQ),
    .BITRATE  (UART_BITRATE)
  )
    serializer_bitrate_ena
    (
      .areset   (areset),
      .clk      (clk),

      .tx_ena   (ser_ena)
    );
    
  UART_serializer
    serializer
    (
      .areset   (areset),
      .clk      (clk),
  
      .ena      (ser_ena),
  
      .d        (ser_d),
      .load     (ser_load),
      .busy     (ser_busy),

      .stx      (ser_tx)
    );

  typedef logic [ 7: 0] uint8_t;
  typedef logic [31: 0] uint32_t;
  
  task automatic send_byte
  (
    input   logic [ 7: 0] x
  );
    ser_d    = x;

    forever @(posedge ser_ena) begin
      ser_load = !ser_busy;

      if (ser_load)
        break;
    end

    @(posedge clk)    

    ser_load = '0;
    ser_d    = 'x;

  endtask : send_byte

  task automatic send_array(input uint8_t x[]);
    
    foreach (x[i])
      send_byte(x[i]);

  endtask : send_array

  virtual class crc #(
    parameter CRC_WIDTH     = 8,
    parameter CRC_POLYNOME  = 8'h07,
    parameter DATA_WIDTH    = 8,

    parameter DATA_REFLECT  = 0,
    parameter CRC_REFLECT   = 0
  );
    typedef logic [DATA_WIDTH-1:0] data_t;
    typedef logic [CRC_WIDTH-1:0]  crc_t;

    static function crc_t  reflect_crc(input crc_t c);
      for (int i = 0; i < CRC_WIDTH; i ++)
        reflect_crc[i] = c[CRC_WIDTH-1 - i];
    endfunction : reflect_crc

    static function crc_t crc_next(input data_t d, input crc_t c);
      crc_t v;
      v = c;    
      for (int i = 0; i < DATA_WIDTH; i ++) begin
        logic b;

        if (DATA_REFLECT != 0) begin
          b = d[0];
          d = d >> 1;
        end
        else begin
          b = d[DATA_WIDTH-1];
          d = d << 1;
        end

        if (CRC_REFLECT != 0) begin
          b = b ^ v[0];
          v = (v >> 1) ^ (reflect_crc(CRC_POLYNOME) & { CRC_WIDTH { b} });
        end
        else begin
          b = b ^ v[CRC_WIDTH-1];
          v = (v << 1) ^ (CRC_POLYNOME & { CRC_WIDTH { b } });
        end
      end

      crc_next = v;
    endfunction : crc_next

  endclass

  task automatic write_memory_block(input logic [23: 0] offset, input uint32_t data[]);
    
    typedef crc#() cm_t;

    cm_t::crc_t   c;
    cm_t::data_t  d;
    
    c = '1;
    d =         8'h55; send_byte(d); c = cm_t::crc_next(d, c);
    d = offset[ 7: 0]; send_byte(d); c = cm_t::crc_next(d, c);
    d = offset[15: 8]; send_byte(d); c = cm_t::crc_next(d, c);
    d = offset[23:16]; send_byte(d); c = cm_t::crc_next(d, c);
    d =   $size(data); send_byte(d); c = cm_t::crc_next(d, c);
    d =             c; send_byte(d); c = cm_t::crc_next(d, c);

    foreach(data[i]) begin
      c = '1;
      d = data[i][ 7: 0]; send_byte(d); c = cm_t::crc_next(d, c);
      d = data[i][15: 8]; send_byte(d); c = cm_t::crc_next(d, c);
      d = data[i][23:16]; send_byte(d); c = cm_t::crc_next(d, c);
      d = data[i][31:24]; send_byte(d); c = cm_t::crc_next(d, c);
      d =              c; send_byte(d); c = cm_t::crc_next(d, c);
    end

  endtask : write_memory_block

  initial begin
    #1000 
    send_array('{ 8'h00, 8'h00, 8'h00, 8'h00, 8'h00, 8'h00 });
    
//     #200
//     write_memory_block(24'h010000, '{ 32'h11111111, 32'h22222222, 32'h33333333, 32'h44444444, 32'h55555555, 32'h66666666, 32'h77777777, 32'h88888888 });
// 
//     #15000
//     write_memory_block(24'h002000, '{ 32'h00000000, 32'h00000002, 
//                                       32'h01000001, 32'h00000003, 
//                                       32'h02000002, 32'h00000004, 
//                                       32'h03000003, 32'h00000005 }
//                       );

    #200
    write_memory_block(24'h010000, '{ 32'h12345678, 32'h87654321 });
    write_memory_block(24'h002002, '{ 32'h00_010000, 32'hFFFF_0000 });

	 #1000000
    write_memory_block(24'h002002, '{ 32'h00_010000, 32'hFFFF_0002 });

	 #5000000
    write_memory_block(24'h002002, '{ 32'h00_010000, 32'hFFFF_0002 });
  end

//
  
  wire          clk48000kHz = clk;

  wire [ 7: 0]  AR429_RX_P;
  wire [ 7: 0]  AR429_RX_N;
  
//  assign AR429_RX_P[7:4]    = '0;
//  assign AR429_RX_N[7:4]    = '0;
      
  wire          AR429_RX_TEST74_P;
  wire          AR429_RX_TEST74_N;
  
  wire          AR429_RX_TEST30_P;
  wire          AR429_RX_TEST30_N;

  // ARINC 429 transmitters pins (HI-8596PS)
  wire [ 3: 0]  AR429_TX_P;
  wire [ 3: 0]  AR429_TX_N;

  assign AR429_RX_P[7:0] = { 4'b0, AR429_TX_P }; // { 8 { ser_tx_a } };
  assign AR429_RX_N[7:0] = { 4'b0, AR429_TX_N }; // { 8 { ser_tx_b } };

  wire [ 3: 0]  AR429_TX_SLP;
  
  // UART pins
  wire          UART_RX = ser_tx;
  wire          UART_TX;
  
  // Asynchronous static RAM
  wire [ 2: 0]  ASRAM_CE_N;
  wire [31: 0]  ASRAM_D;
  wire [18: 0]  ASRAM_A;
  wire [ 3: 0]  ASRAM_BE_N;
  wire          ASRAM_WE_N;
  wire          ASRAM_OE_N;

  CY7C1062_tb1
    ram
    (
      .ASRAM_CE_N (ASRAM_CE_N),
      .ASRAM_D    (ASRAM_D),
      .ASRAM_A    (ASRAM_A),
      .ASRAM_BE_N (ASRAM_BE_N),
      .ASRAM_WE_N (ASRAM_WE_N),
      .ASRAM_OE_N (ASRAM_OE_N)
    );

  wire AR429_RX_LED;
  wire DS18B20_TRAM;
  wire DS18B20_TAUX;

  aubridge
    aubridge_top
    (
      .*
    );

endmodule
