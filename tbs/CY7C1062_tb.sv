module CY7C1062_tb
(
  input   wire [ 2: 0]  ASRAM_CE_N,
  inout   wire [31: 0]  ASRAM_D,
  input   wire [18: 0]  ASRAM_A,
  input   wire [ 3: 0]  ASRAM_BE_N,
  input   wire          ASRAM_WE_N,
  input   wire          ASRAM_OE_N
);

  bit [524288: 0][31: 0]  ram;
  
  initial begin
    ram = '0;
    
    ram[19'h02000] = 32'h0000_0000;
    ram[19'h02001] = 32'h0000_0001;
    
    ram[19'h10000] = 32'h1234_5678;
  end

  logic [31: 0] d_out;
    
  always@(ASRAM_CE_N or ASRAM_WE_N or ASRAM_OE_N or ASRAM_BE_N or ASRAM_A or ASRAM_D) begin
    if (ASRAM_CE_N == '0) begin

      if (ASRAM_WE_N == '0 && ASRAM_OE_N == '1) begin
        if (ASRAM_BE_N[0] == '0) ram[ASRAM_A][ 7: 0] = ASRAM_D[ 7: 0];
        if (ASRAM_BE_N[1] == '0) ram[ASRAM_A][15: 8] = ASRAM_D[15: 8];
        if (ASRAM_BE_N[2] == '0) ram[ASRAM_A][23:16] = ASRAM_D[23:16];
        if (ASRAM_BE_N[3] == '0) ram[ASRAM_A][31:24] = ASRAM_D[31:24];          
      end

      d_out = ram[ASRAM_A];
      /*
      if (ASRAM_OE_N == '0 && ASRAM_WE_N == '1) begin
        if (ASRAM_BE_N[0] == '0) d_out[ 7: 0] = ram[ASRAM_A][ 7: 0];
        if (ASRAM_BE_N[1] == '0) d_out[15: 8] = ram[ASRAM_A][15: 8];
        if (ASRAM_BE_N[2] == '0) d_out[23:16] = ram[ASRAM_A][23:16];
        if (ASRAM_BE_N[3] == '0) d_out[31:24] = ram[ASRAM_A][31:24];          
      end
      */
      
    end
    
  end

  assign ASRAM_D[ 7: 0] = ( (ASRAM_CE_N == '0) && (ASRAM_OE_N == '0) && (ASRAM_WE_N == '1) && (ASRAM_BE_N[0] == '0) ) ? d_out[ 7: 0] : 8'bZ;
  assign ASRAM_D[15: 8] = ( (ASRAM_CE_N == '0) && (ASRAM_OE_N == '0) && (ASRAM_WE_N == '1) && (ASRAM_BE_N[1] == '0) ) ? d_out[15: 8] : 8'bZ;
  assign ASRAM_D[23:16] = ( (ASRAM_CE_N == '0) && (ASRAM_OE_N == '0) && (ASRAM_WE_N == '1) && (ASRAM_BE_N[2] == '0) ) ? d_out[23:16] : 8'bZ;
  assign ASRAM_D[31:24] = ( (ASRAM_CE_N == '0) && (ASRAM_OE_N == '0) && (ASRAM_WE_N == '1) && (ASRAM_BE_N[3] == '0) ) ? d_out[31:24] : 8'bZ;

endmodule
