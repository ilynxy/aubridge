onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /aubridge_tb/aubridge_top/clk48000kHz
add wave -noupdate /aubridge_tb/aubridge_top/areset
add wave -noupdate /aubridge_tb/aubridge_top/clk
add wave -noupdate -radix hexadecimal /aubridge_tb/aubridge_top/ASRAM_A
add wave -noupdate /aubridge_tb/aubridge_top/ASRAM_BE_N
add wave -noupdate /aubridge_tb/aubridge_top/ASRAM_CE_N
add wave -noupdate /aubridge_tb/aubridge_top/ASRAM_OE_N
add wave -noupdate /aubridge_tb/aubridge_top/ASRAM_WE_N
add wave -noupdate -radix hexadecimal /aubridge_tb/aubridge_top/ASRAM_D
add wave -noupdate /aubridge_tb/aubridge_top/UART_TX
add wave -noupdate /aubridge_tb/aubridge_top/a429_divider_enas/ena_1ms
add wave -noupdate /aubridge_tb/aubridge_top/frame_generator/state
add wave -noupdate /aubridge_tb/aubridge_top/frame_generator/next_state
add wave -noupdate -radix hexadecimal /aubridge_tb/aubridge_top/frame_generator/ser_d
add wave -noupdate -radix hexadecimal {/aubridge_tb/aubridge_top/a429_to_fifo/a429_rcv[0]/rcv/q}
add wave -noupdate {/aubridge_tb/aubridge_top/a429_to_fifo/a429_rcv[0]/rcv/valid}
add wave -noupdate /aubridge_tb/aubridge_top/UART_RX
add wave -noupdate /aubridge_tb/aubridge_top/command_parser/state
add wave -noupdate /aubridge_tb/aubridge_top/command_parser/next_state
add wave -noupdate -radix hexadecimal /aubridge_tb/aubridge_top/command_parser/deser_q
add wave -noupdate -radix hexadecimal /aubridge_tb/aubridge_top/command_parser/bc
add wave -noupdate -radix hexadecimal /aubridge_tb/aubridge_top/command_parser/crc
add wave -noupdate -radix hexadecimal /aubridge_tb/aubridge_top/deser_q
add wave -noupdate /aubridge_tb/aubridge_top/deser_valid
add wave -noupdate /aubridge_tb/aubridge_top/uart_deser/bc
add wave -noupdate -radix hexadecimal /aubridge_tb/aubridge_top/command_parser/wcnt
add wave -noupdate -expand /aubridge_tb/AR429_TX_P
add wave -noupdate -expand /aubridge_tb/AR429_TX_N
add wave -noupdate -expand /aubridge_tb/aubridge_top/a429_play/ser_load
add wave -noupdate -radix hexadecimal -childformat {{{/aubridge_tb/aubridge_top/a429_play/ser_d[3]} -radix hexadecimal} {{/aubridge_tb/aubridge_top/a429_play/ser_d[2]} -radix hexadecimal} {{/aubridge_tb/aubridge_top/a429_play/ser_d[1]} -radix hexadecimal} {{/aubridge_tb/aubridge_top/a429_play/ser_d[0]} -radix hexadecimal}} -expand -subitemconfig {{/aubridge_tb/aubridge_top/a429_play/ser_d[3]} {-radix hexadecimal} {/aubridge_tb/aubridge_top/a429_play/ser_d[2]} {-radix hexadecimal} {/aubridge_tb/aubridge_top/a429_play/ser_d[1]} {-radix hexadecimal} {/aubridge_tb/aubridge_top/a429_play/ser_d[0]} {-radix hexadecimal}} /aubridge_tb/aubridge_top/a429_play/ser_d
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {9999562307 ps} 0} {{Cursor 2} {4996786537 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 388
configure wave -valuecolwidth 121
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 41666
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {10500 us}
