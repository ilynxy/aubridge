
--************************************************************************
--**    MODEL   :       async_512kx32.vhd     	                         **
--**    COMPANY :       Cypress Semiconductor
--**    DATE    :       September 22,  2009  
--**    REVISION:         1.2                                          **
--**    REVISION HISTORY: 1.2  added WE_b and OE_b to read_enable signal
--**			  1.1  Updated model (Changed CE2 to CE2_b)                   **
--**                      1.0  New Base model                           **
--************************************************************************

Library IEEE,work;
Use IEEE.Std_Logic_1164.All;
use IEEE.Std_Logic_unsigned.All;

use work.package_timing.all;
use work.package_utility.all;

------------------------
-- Entity Description
------------------------

Entity async_512kx32 is
generic
	(ADDR_BITS			: integer := 19;
	 DATA_BITS			 : integer := 32;
	 depth 				 : integer := 524288;	
	 TimingInfo			: BOOLEAN := TRUE;
	 TimingChecks	: std_logic := '1'
	);
Port (
    CE1_b   : IN Std_Logic;	                                                -- Chip Enable CE1#
    CE2_b   : IN Std_Logic;	                                                -- Chip Enable CE2_b    
    CE3_b   : IN Std_Logic;	                                                -- Chip Enable CE3#    
    WE_b  	: IN Std_Logic;	                                                -- Write Enable WE#
    OE_b  	: IN Std_Logic;                                                 -- Output Enable OE#
    Ba_b		: IN std_logic;                                                 -- Byte Enable Low
    Bb_b  : IN std_logic;                                                 -- Byte Enable Low 
    Bc_b		: IN std_logic;                                                 -- Byte Enable Low
    Bd_b  : IN std_logic;                                                 -- Byte Enable Low 
    A 			  : IN Std_Logic_Vector(addr_bits-1 downto 0);                    -- Address Inputs A
    DQ			  : INOUT Std_Logic_Vector(DATA_BITS-1 downto 0):=(others=>'Z')   -- Read/Write Data IO
    ); 
End async_512kx32;

-----------------------------
-- End Entity Description
-----------------------------
-----------------------------
-- Architecture Description
-----------------------------

Architecture behave_arch Of async_512kx32 Is

Type mem_array_type Is array (depth-1 downto 0) of std_logic_vector(DATA_BITS-1 downto 0);

signal write_enable : std_logic;
signal read_enable : std_logic;
signal byte_enable : std_logic;
signal CE_b : std_logic;
signal data_skew : Std_Logic_Vector(DATA_BITS-1 downto 0);

signal address_internal,address_skew: Std_Logic_Vector(addr_bits-1 downto 0);

constant tSD_dataskew : time := tSD - 1 ns;
constant tskew :time := 1 ns;

begin 

CE_b <=  not(not(CE1_b) and not(CE3_b) and not (CE2_b)) ;
byte_enable <= not(Ba_b and Bb_b and Bc_b and Bd_b);
write_enable <= not(CE1_b) and not(CE3_b) and not(CE2_b) and not(WE_b) and not(Ba_b and Bb_b and Bc_b and Bd_b);
read_enable <= not(CE1_b) and not(CE3_b) and not(CE2_b) and not(Ba_b and Bb_b and Bc_b and Bd_b) and (WE_b) and not(OE_b);
data_skew <= DQ after 1 ns; -- changed on feb 15
address_skew <= A after 1 ns; 

process (OE_b) 
begin
    if (OE_b'event and OE_b = '1' and write_enable /= '1') then
        DQ <=(others=>'Z') after tHZOE;
    end if; 
end process;

process (CE_b)
begin
    if (CE_b'event and CE_b = '1') then
        DQ <=(others=>'Z') after tHZCE;
    end if;
end process;

process (write_enable'delayed(tHA))
begin
    if (write_enable'delayed(tHA) = '0' and TimingInfo) then
	assert (A'last_event = 0 ns) or (A'last_event > tHA)
	report "Address hold time tHA violated";
    end if;
end process;

process (write_enable'delayed(tHD))
begin
    if (write_enable'delayed(tHD) = '0' and TimingInfo) then
	assert (DQ'last_event > tHD) or (DQ'last_event = 0 ns)
	report "Data hold time tHD violated";
    end if;
end process;

-- main process
process
    
VARIABLE mem_array: mem_array_type;

--- Variables for timing checks
VARIABLE tPWE_chk : TIME := -10 ns;
VARIABLE tAW_chk : TIME := -10 ns;
VARIABLE tSD_chk : TIME := -10 ns;
VARIABLE tRC_chk : TIME := 0 ns;
VARIABLE tBAW_chk : TIME := 0 ns;
VARIABLE tBBW_chk : TIME := 0 ns;
VARIABLE tBCW_chk : TIME := 0 ns;
VARIABLE tBDW_chk : TIME := 0 ns;
VARIABLE tSA_chk : TIME := 0 ns;
VARIABLE tSA_skew : TIME := 0 ns;
VARIABLE tAint_chk : TIME := -10 ns;

VARIABLE write_flag : BOOLEAN := TRUE;

VARIABLE accesstime : TIME := 0 ns;
    
begin
   if (address_skew'event) then
          tSA_skew := NOW;
   end if;

    -- start of write
    if (write_enable = '1' and write_enable'event) then
             
       DQ(DATA_BITS-1 downto 0)<=(others=>'Z') after tHZWE;

       if (A'last_event >= tSA) then
          address_internal <= A;
          tPWE_chk := NOW;
          tAW_chk := A'last_event;
          tAint_chk := NOW;
          write_flag := TRUE;

       else
          if (TimingInfo) then
		       assert FALSE
		       report "Address setup violated";
	       end if;
          write_flag := FALSE;

       end if;   
    
    -- end of write (with CE high or WE high)
    elsif (write_enable = '0' and write_enable'event) then

        --- check for pulse width
        if (NOW - tPWE_chk >= tPWE or NOW - tPWE_chk <= 0.1 ns or NOW = 0 ns) then
            --- pulse width OK, do nothing
        else
      	   if (TimingInfo) then
           	assert FALSE
		      report "Pulse Width violation";
	       end if;
	     
	     write_flag := FALSE;
	     end if;
        if (NOW > 0 ns) then
          if (tSA_skew - tAint_chk > tskew ) then
               assert FALSE
               report "Negative address setup";
               write_flag := FALSE;
          end if;                
       end if;
        
        --- check for address setup with write end, i.e., tAW
        if (NOW - tAW_chk >= tAW or NOW = 0 ns) then
            --- tAW OK, do nothing
        else
      	   if (TimingInfo) then
	         assert FALSE
		      report "Address setup tAW violation";
	       end if;

          write_flag := FALSE;
        end if;
        
        --- check for data setup with write end, i.e., tSD
        if (NOW - tSD_chk >= tSD_dataskew or NOW - tSD_chk <= 0.1 ns or NOW = 0 ns) then 
            --- tSD OK, do nothing
        else
      	   if (TimingInfo) then
	          assert FALSE
	   	      report "Data setup tSD violation";
	       end if;
          write_flag := FALSE;
        end if;
        
        -- perform write operation if no violations
        if (write_flag = TRUE) then

            if (Ba_b = '1' and Ba_b'last_event = write_enable'last_event and NOW /= 0 ns) then
            	mem_array(conv_integer1(address_internal))(7 downto 0) := data_skew(7 downto 0);
            end if;
            
            if (Bb_b = '1' and Bb_b'last_event = write_enable'last_event and NOW /= 0 ns) then
            	mem_array(conv_integer1(address_internal))(15 downto 8) := data_skew(15 downto 8);
            end if;

            if (Bc_b = '1' and Bc_b'last_event = write_enable'last_event and NOW /= 0 ns) then
            	mem_array(conv_integer1(address_internal))(23 downto 16) := data_skew(23 downto 16);
            end if;
            
            if (Bd_b = '1' and Bd_b'last_event = write_enable'last_event and NOW /= 0 ns) then
            	mem_array(conv_integer1(address_internal))(31 downto 24) := data_skew(31 downto 24);
            end if;

            if (Ba_b = '0' and NOW - tBAW_chk >= tBW) then
            	mem_array(conv_integer1(address_internal))(7 downto 0) := data_skew(7 downto 0);
            elsif (NOW - tBAW_chk < tBW and NOW - tBAW_chk > 0.1 ns and NOW > 0 ns) then
            	assert FALSE report "Insufficient pulse width for lower byte to be written";
            end if;
            	
            if (Bb_b = '0' and NOW - tBBW_chk >= tBW) then
            	mem_array(conv_integer1(address_internal))(15 downto 8) := data_skew(15 downto 8);
            elsif (NOW - tBBW_chk < tBW and NOW - tBBW_chk > 0.1 ns and NOW > 0 ns) then
            	assert FALSE report "Insufficient pulse width for the I/Os 15 downto 8 to be written";
            end if;

            if (Bc_b = '0' and NOW - tBCW_chk >= tBW) then
            	mem_array(conv_integer1(address_internal))(23 downto 16) := data_skew(23 downto 16);
            elsif (NOW - tBCW_chk < tBW and NOW - tBCW_chk > 0.1 ns and NOW > 0 ns) then
            	assert FALSE report "Insufficient pulse width for the I/Os 23 downto 16 to be written";
            end if;
            	
            if (Bd_b = '0' and NOW - tBDW_chk >= tBW) then
            	mem_array(conv_integer1(address_internal))(31 downto 24) := data_skew(31 downto 24);
            elsif (NOW - tBDW_chk < tBW and NOW - tBDW_chk > 0.1 ns and NOW > 0 ns) then
            	assert FALSE report "Insufficient pulse width for higher byte to be written";
            end if;

        end if; 

    -- end of write (with Ba_b high)
    elsif (Ba_b'event and (not(Bb_b'event) or not(Bc_b'event) or not(Bd_b'event)) and write_enable = '1') then
    
       if (Ba_b = '0') then
     
      	   --- Reset timing variables
          tAW_chk := A'last_event;
          tBAW_chk := NOW;
          write_flag := TRUE;
     
       elsif (Ba_b = '1') then
        
          --- check for pulse width
          if (NOW - tPWE_chk >= tPWE) then
            --- tPWE OK, do nothing
          else
      	      if (TimingInfo) then
            	   assert FALSE
		          report "Pulse Width violation";
	          end if;

	          write_flag := FALSE;
	       end if;
        
           --- check for address setup with write end, i.e., tAW
           if (NOW - tAW_chk >= tAW) then
            --- tAW OK, do nothing
           else
      	       if (TimingInfo) then
	              assert FALSE
		           report "Address setup tAW violation for Lower Byte Write";
	           end if;

              write_flag := FALSE;
           end if;
	
           --- check for byte write setup with write end, i.e., tBW
           if (NOW - tBAW_chk >= tBW) then
            --- tBW OK, do nothing
           else
      	       if (TimingInfo) then
                 assert FALSE
		           report "Lower Byte setup tBW violation";
	           end if;

              write_flag := FALSE;
           end if;
	
	        --- check for data setup with write end, i.e., tSD
           if (NOW - tSD_chk >= tSD_dataskew or NOW - tSD_chk <= 0.1 ns or NOW = 0 ns) then 
            --- tSD OK, do nothing
           else
      	       if (TimingInfo) then
	              assert FALSE
	   	          report "Data setup tSD violation for Lower Byte Write";
	           end if;
           
              write_flag := FALSE;
           end if;
	
	        --- perform WRITE operation if no violations
	        if (write_flag = TRUE) then
	           mem_array(conv_integer1(address_internal))(7 downto 0) := data_skew(7 downto 0);
              if (Bb_b = '0') then
            	    mem_array(conv_integer1(address_internal))(15 downto 8) := data_skew(15 downto 8);
              end if;
              if (Bc_b = '0') then
            	    mem_array(conv_integer1(address_internal))(23 downto 16) := data_skew(23 downto 16);
              end if;
              if (Bd_b = '0') then
            	    mem_array(conv_integer1(address_internal))(31 downto 24) := data_skew(31 downto 24);
              end if;              
	        end if;
	
   	       --- Reset timing variables
           tAW_chk := A'last_event;
           tBAW_chk := NOW;
           write_flag := TRUE;
      
      end if;

    -- end of write (with Bb_b high)
    elsif (Bb_b'event and (not(Ba_b'event) or not(Bc_b'event) or not(Bd_b'event)) and write_enable = '1') then

      if (Bb_b = '0') then
     
    	   --- Reset timing variables
        tAW_chk := A'last_event;
        tBBW_chk := NOW;
        write_flag := TRUE;
     
      elsif (Bb_b = '1') then
        
        --- check for pulse width
        if (NOW - tPWE_chk >= tPWE) then
            --- tPWE OK, do nothing
        else
      	   if (TimingInfo) then
           	assert FALSE
		      report "Pulse Width violation";
	       end if;
	     
	     write_flag := FALSE;
	     end if;
        
        --- check for address setup with write end, i.e., tAW
        if (NOW - tAW_chk >= tAW) then
            --- tAW OK, do nothing
        else
      	   if (TimingInfo) then
	         assert FALSE
		      report "Address setup tAW violation for Byte Write (I/O23 - I/O16)";
	       end if;
          write_flag := FALSE;
        end if;
	
        --- check for byte setup with write end, i.e., tBW
        if (NOW - tBBW_chk >= tBW) then
            --- tBW OK, do nothing
        else
      	   if (TimingInfo) then
	         assert FALSE
		      report "Byte setup (I/O23-16) tBW violation";
	       end if;
        
        write_flag := FALSE;
        end if;
	
        --- check for data setup with write end, i.e., tSD
        if (NOW - tSD_chk >= tSD_dataskew or NOW - tSD_chk <= 0.1 ns or NOW = 0 ns) then
            --- tSD OK, do nothing
        else
      	   if (TimingInfo) then
	          assert FALSE
	   	      report "Data setup tSD violation for Byte Write (I/O23-16)";
	       end if;
        
          write_flag := FALSE;
        end if;
	
	     --- perform WRITE operation if no violations
	
	     if (write_flag = TRUE) then
	       mem_array(conv_integer1(address_internal))(15 downto 8) := data_skew(15 downto 8);
		          if (Ba_b = '0') then
    		        	mem_array(conv_integer1(address_internal))(7 downto 0) := data_skew(7 downto 0);
        		  end if;
              if (Bc_b = '0') then
            	    mem_array(conv_integer1(address_internal))(23 downto 16) := data_skew(23 downto 16);
              end if;
              if (Bd_b = '0') then
            	    mem_array(conv_integer1(address_internal))(31 downto 24) := data_skew(31 downto 24);
              end if;              
	      	 end if; 
	
	     --- Reset timing variables
	     tAW_chk := A'last_event;
        tBBW_chk := NOW;
        write_flag := TRUE;
	    
     end if;

    -- end of write (with Bb_b high)
    elsif (Bc_b'event and (not(Ba_b'event) or not(Bb_b'event) or not(Bd_b'event)) and write_enable = '1') then

      if (Bc_b = '0') then
     
    	   --- Reset timing variables
        tAW_chk := A'last_event;
        tBCW_chk := NOW;
        write_flag := TRUE;
     
      elsif (Bc_b = '1') then
        
        --- check for pulse width
        if (NOW - tPWE_chk >= tPWE) then
            --- tPWE OK, do nothing
        else
      	   if (TimingInfo) then
           	assert FALSE
		      report "Pulse Width violation";
	       end if;
	     
	     write_flag := FALSE;
	     end if;
        
        --- check for address setup with write end, i.e., tAW
        if (NOW - tAW_chk >= tAW) then
            --- tAW OK, do nothing
        else
      	   if (TimingInfo) then
	         assert FALSE
		      report "Address setup tAW violation for Byte Write (I/O23 - I/O16)";
	       end if;
          write_flag := FALSE;
        end if;
	
        --- check for byte setup with write end, i.e., tBW
        if (NOW - tBCW_chk >= tBW) then
            --- tBW OK, do nothing
        else
      	   if (TimingInfo) then
	         assert FALSE
		      report "Byte setup (I/O23-16) tBW violation";
	       end if;
        
        write_flag := FALSE;
        end if;
	
        --- check for data setup with write end, i.e., tSD
        if (NOW - tSD_chk >= tSD_dataskew or NOW - tSD_chk <= 0.1 ns or NOW = 0 ns) then
            --- tSD OK, do nothing
        else
      	   if (TimingInfo) then
	          assert FALSE
	   	      report "Data setup tSD violation for Byte Write (I/O23-16)";
	       end if;
        
          write_flag := FALSE;
        end if;
	
	     --- perform WRITE operation if no violations
	
	     if (write_flag = TRUE) then
	       mem_array(conv_integer1(address_internal))(15 downto 8) := data_skew(15 downto 8);
		          if (Ba_b = '0') then
    		        	mem_array(conv_integer1(address_internal))(7 downto 0) := data_skew(7 downto 0);
        		  end if;
              if (Bb_b = '0') then
            	    mem_array(conv_integer1(address_internal))(15 downto 8) := data_skew(15 downto 8);
              end if;
              if (Bd_b = '0') then
            	    mem_array(conv_integer1(address_internal))(31 downto 24) := data_skew(31 downto 24);
              end if;              
	      	 end if; 
	
	     --- Reset timing variables
	     tAW_chk := A'last_event;
        tBCW_chk := NOW;
        write_flag := TRUE;
	    
     end if;
    -- end of write (with Bc_b high)
    elsif (Bd_b'event and (not(Ba_b'event) or not(Bb_b'event) or not(Bc_b'event)) and write_enable = '1') then

      if (Bd_b = '0') then
     
    	   --- Reset timing variables
        tAW_chk := A'last_event;
        tBDW_chk := NOW;
        write_flag := TRUE;
     
      elsif (Bd_b = '1') then
        
        --- check for pulse width
        if (NOW - tPWE_chk >= tPWE) then
            --- tPWE OK, do nothing
        else
      	   if (TimingInfo) then
           	assert FALSE
		      report "Pulse Width violation";
	       end if;
	     
	     write_flag := FALSE;
	     end if;
        
        --- check for address setup with write end, i.e., tAW
        if (NOW - tAW_chk >= tAW) then
            --- tAW OK, do nothing
        else
      	   if (TimingInfo) then
	         assert FALSE
		      report "Address setup tAW violation for Byte Write (I/O31 - 24)";
	       end if;
          write_flag := FALSE;
        end if;
	
        --- check for byte setup with write end, i.e., tBW
        if (NOW - tBDW_chk >= tBW) then
            --- tBW OK, do nothing
        else
      	   if (TimingInfo) then
	         assert FALSE
		      report "Byte setup (I/O31 - 24) tBW violation";
	       end if;
        
        write_flag := FALSE;
        end if;
	
        --- check for data setup with write end, i.e., tSD
        if (NOW - tSD_chk >= tSD_dataskew or NOW - tSD_chk <= 0.1 ns or NOW = 0 ns) then
            --- tSD OK, do nothing
        else
      	   if (TimingInfo) then
	          assert FALSE
	   	      report "Data setup tSD violation for Upper Byte Write (I/O31-24)";
	       end if;
        
          write_flag := FALSE;
        end if;
	
	     --- perform WRITE operation if no violations
	
	     if (write_flag = TRUE) then
	       mem_array(conv_integer1(address_internal))(31 downto 24) := data_skew(31 downto 24);
		          if (Ba_b = '0') then
    		        	mem_array(conv_integer1(address_internal))(7 downto 0) := data_skew(7 downto 0);
        		  end if;
              if (Bb_b = '0') then
            	    mem_array(conv_integer1(address_internal))(15 downto 8) := data_skew(15 downto 8);
              end if;                     		  
              if (Bc_b = '0') then
            	    mem_array(conv_integer1(address_internal))(23 downto 16) := data_skew(23 downto 16);
              end if;
	      	 end if; 
	
	     --- Reset timing variables
	     tAW_chk := A'last_event;
        tBDW_chk := NOW;
        write_flag := TRUE;
	    
     end if;

  end if;
  --- END OF WRITE

  if (data_skew'event and read_enable /= '1') then
    	tSD_chk := NOW;
  end if;

  --- START of READ
    
  --- Tri-state the data bus if CE or OE disabled
  if (read_enable = '0' and read_enable'event) then
    if (OE_b'last_event >= CE_b'last_event) then
   		DQ <=(others=>'Z') after tHZCE;
   	elsif (CE_b'last_event > OE_b'last_event) then
   		DQ <=(others=>'Z') after tHZOE;
   	end if;
  end if;
   
  --- Address-controlled READ operation
  if (A'event) then
    if (A'last_event = CE_b'last_event and CE_b = '1') then
       DQ <=(others=>'Z') after tHZCE;
    end if;
       
    if (NOW - tRC_chk >= tRC or NOW - tRC_chk <= 0.1 ns or tRC_chk = 0 ns) then
      --- tRC OK, do nothing
    else
           
       if (TimingInfo) then
	       assert FALSE
	   	   report "Read Cycle time tRC violation";
	    end if;

    end if;    
       
    if (read_enable = '1') then
	   
	   if (Ba_b = '0') then
		   DQ (7 downto 0) <= mem_array (conv_integer1(A))(7 downto 0) after tAA;
	   end if;
	   
	   if (Bb_b = '0') then
		   DQ (15 downto 8) <= mem_array (conv_integer1(A))(15 downto 8) after tAA;
	   end if;
	   
	   if (Bc_b = '0') then
		   DQ (23 downto 16) <= mem_array (conv_integer1(A))(23 downto 16) after tAA;
	   end if;
	   
	   if (Bb_b = '0') then
		   DQ (31 downto 24) <= mem_array (conv_integer1(A))(31 downto 24) after tAA;
	   end if;
	   
      tRC_chk := NOW;

	end if;
	
	if (write_enable = '1') then
	   --- do nothing
	end if;
	
  end if;

  if (read_enable = '0' and read_enable'event) then
     DQ <=(others=>'Z') after tHZCE;
     if (NOW - tRC_chk >= tRC or tRC_chk = 0 ns or A'last_event = read_enable'last_event) then 
     --- tRC_chk needs to be reset when read ends
        tRC_CHK := 0 ns;
     else
         if (TimingInfo) then
	        assert FALSE
		     report "Read Cycle time tRC violation";
 	      end if;          
	      tRC_CHK := 0 ns;
     end if;

   end if;

   --- READ operation triggered by CE/OE/Bb/Ba
   if (read_enable = '1' and read_enable'event) then

      tRC_chk := NOW;

       --- CE triggered READ
       if (CE_b'last_event = read_enable'last_event ) then --  changed rev2

           if (Ba_b = '0') then
		         DQ (7 downto 0)<= mem_array (conv_integer1(A)) (7 downto 0) after tACE;
           end if;

           if (Bb_b = '0') then
		         DQ (15 downto 8)<= mem_array (conv_integer1(A)) (15 downto 8) after tACE;
           end if;

           if (Bc_b = '0') then
		         DQ (23 downto 16)<= mem_array (conv_integer1(A)) (23 downto 16) after tACE;
           end if;

           if (Bd_b = '0') then
		         DQ (31 downto 24)<= mem_array (conv_integer1(A)) (31 downto 24) after tACE;
           end if;
           
       end if;

   
 	    --- OE triggered READ  
       if (OE_b'last_event = read_enable'last_event) then

           -- if address or CE changes before OE such that tAA/tACE > tDOE
           if (CE_b'last_event < tACE - tDOE and A'last_event < tAA - tDOE) then
               
               if (A'last_event < CE_b'last_event) then

                  accesstime:=tAA-A'last_event;
                  if (Ba_b = '0') then
		               DQ (7 downto 0)<= mem_array (conv_integer1(A)) (7 downto 0) after accesstime;
                  end if; 
               
                  if (Bb_b = '0') then
   		               DQ (15 downto 8)<= mem_array (conv_integer1(A)) (15 downto 8) after accesstime;
                  end if;               

                  if (Bc_b = '0') then
		               DQ (23 downto 16)<= mem_array (conv_integer1(A)) (23 downto 16) after accesstime;
                  end if; 
               
                  if (Bd_b = '0') then
   		               DQ (31 downto 24)<= mem_array (conv_integer1(A)) (31 downto 24) after accesstime;
                  end if;               

               else
                  accesstime:=tACE-CE_b'last_event;
                  if (Ba_b = '0') then
		               DQ (7 downto 0)<= mem_array (conv_integer1(A)) (7 downto 0) after accesstime;
                  end if; 
               
                  if (Bb_b = '0') then
   		               DQ (15 downto 8)<= mem_array (conv_integer1(A)) (15 downto 8) after accesstime;
                  end if;

                  if (Bc_b = '0') then
		               DQ (23 downto 16)<= mem_array (conv_integer1(A)) (23 downto 16) after accesstime;
                  end if; 
               
                  if (Bd_b = '0') then
   		               DQ (31 downto 24)<= mem_array (conv_integer1(A)) (31 downto 24) after accesstime;
                  end if;

              end if;

           -- if address changes before OE such that tAA > tDOE
           elsif (A'last_event < tAA - tDOE) then
               
                  accesstime:=tAA-A'last_event;
                  if (Ba_b = '0') then
		               DQ (7 downto 0)<= mem_array (conv_integer1(A)) (7 downto 0) after accesstime;
                  end if; 
               
                  if (Bb_b = '0') then
   		               DQ (15 downto 8)<= mem_array (conv_integer1(A)) (15 downto 8) after accesstime;
                  end if;

                  if (Bc_b = '0') then
		               DQ (23 downto 16)<= mem_array (conv_integer1(A)) (23 downto 16) after accesstime;
                  end if; 
               
                  if (Bd_b = '0') then
   		               DQ (31 downto 24)<= mem_array (conv_integer1(A)) (31 downto 24) after accesstime;
                  end if;

           -- if CE changes before OE such that tACE > tDOE
           elsif (CE_b'last_event < tACE - tDOE) then
               
                  accesstime:=tACE-CE_b'last_event;
                  if (Ba_b = '0') then
		               DQ (7 downto 0)<= mem_array (conv_integer1(A)) (7 downto 0) after accesstime;
                  end if; 
               
                  if (Bb_b = '0') then
   		               DQ (15 downto 8)<= mem_array (conv_integer1(A)) (15 downto 8) after accesstime;
                  end if;

                  if (Bc_b = '0') then
		               DQ (23 downto 16)<= mem_array (conv_integer1(A)) (23 downto 16) after accesstime;
                  end if; 
               
                  if (Bd_b = '0') then
   		               DQ (31 downto 24)<= mem_array (conv_integer1(A)) (31 downto 24) after accesstime;
                  end if;

           -- if OE changes such that tDOE > tAA/tACE           
           else
                   if (Ba_b = '0') then
		               DQ (7 downto 0)<= mem_array (conv_integer1(A)) (7 downto 0) after tDOE;
                   end if; 
               
                   if (Bb_b = '0') then
   		               DQ (15 downto 8)<= mem_array (conv_integer1(A)) (15 downto 8) after tDOE;
                   end if;
                   
                  if (Bc_b = '0') then
		               DQ (23 downto 16)<= mem_array (conv_integer1(A)) (23 downto 16) after tDOE;
                  end if; 
               
                  if (Bd_b = '0') then
   		               DQ (31 downto 24)<= mem_array (conv_integer1(A)) (31 downto 24) after tDOE;
                  end if;                
            
           end if;
           
       end if;
       --- END of OE triggered READ

 	    --- Ba/Bb/Bc/Bd triggered READ  
       if (Ba_b'last_event = read_enable'last_event or Bb_b'last_event = read_enable'last_event or Bc_b'last_event = read_enable'last_event or Bd_b'last_event = read_enable'last_event) then

           -- if address or CE changes before Bb/Ba such that tAA/tACE > tDBE
           if (CE_b'last_event < tACE - tDBE and A'last_event < tAA - tDBE) then
               
               if (A'last_event < Ba_b'last_event) then
                  accesstime:=tAA-A'last_event;

                  if (Ba_b = '0') then
		               DQ (7 downto 0)<= mem_array (conv_integer1(A)) (7 downto 0) after accesstime;
                  end if; 
               
                  if (Bb_b = '0') then
   		               DQ (15 downto 8)<= mem_array (conv_integer1(A)) (15 downto 8) after accesstime;
                  end if;

                  if (Bc_b = '0') then
		               DQ (23 downto 16)<= mem_array (conv_integer1(A)) (23 downto 16) after accesstime;
                  end if; 
               
                  if (Bd_b = '0') then
   		               DQ (31 downto 24)<= mem_array (conv_integer1(A)) (31 downto 24) after accesstime;
                  end if;

               else
                  accesstime:=tACE-CE_b'last_event;

                  if (Ba_b = '0') then
		               DQ (7 downto 0)<= mem_array (conv_integer1(A)) (7 downto 0) after accesstime;
                  end if; 
               
                  if (Bb_b = '0') then
   		               DQ (15 downto 8)<= mem_array (conv_integer1(A)) (15 downto 8) after accesstime;
                  end if;

                  if (Bc_b = '0') then
		               DQ (23 downto 16)<= mem_array (conv_integer1(A)) (23 downto 16) after accesstime;
                  end if; 
               
                  if (Bd_b = '0') then
   		               DQ (31 downto 24)<= mem_array (conv_integer1(A)) (31 downto 24) after accesstime;
                  end if;
              end if;

           -- if address changes before Bb/Ba such that tAA > tDBE
           elsif (A'last_event < tAA - tDBE) then
                  accesstime:=tAA-A'last_event;

                  if (Ba_b = '0') then
		               DQ (7 downto 0)<= mem_array (conv_integer1(A)) (7 downto 0) after accesstime;
                  end if; 
               
                  if (Bb_b = '0') then
   		               DQ (15 downto 8)<= mem_array (conv_integer1(A)) (15 downto 8) after accesstime;
                  end if;

                  if (Bc_b = '0') then
		               DQ (23 downto 16)<= mem_array (conv_integer1(A)) (23 downto 16) after accesstime;
                  end if; 
               
                  if (Bd_b = '0') then
   		               DQ (31 downto 24)<= mem_array (conv_integer1(A)) (31 downto 24) after accesstime;
                  end if;

           -- if CE changes before Bb/Ba such that tACE > tDBE
           elsif (CE_b'last_event < tACE - tDBE) then
                  accesstime:=tACE-CE_b'last_event;

                  if (Ba_b = '0') then
		               DQ (7 downto 0)<= mem_array (conv_integer1(A)) (7 downto 0) after accesstime;
                  end if; 
               
                  if (Bb_b = '0') then
   		               DQ (15 downto 8)<= mem_array (conv_integer1(A)) (15 downto 8) after accesstime;
                  end if;

                  if (Bc_b = '0') then
		               DQ (23 downto 16)<= mem_array (conv_integer1(A)) (23 downto 16) after accesstime;
                  end if; 
               
                  if (Bd_b = '0') then
   		               DQ (31 downto 24)<= mem_array (conv_integer1(A)) (31 downto 24) after accesstime;
                  end if;

           -- if Bb/Ba changes such that tDBE > tAA/tACE   
           else
                  if (Ba_b = '0') then
		               DQ (7 downto 0)<= mem_array (conv_integer1(A)) (7 downto 0) after tDBE;
                  end if; 
               
                  if (Bb_b = '0') then
   		               DQ (15 downto 8)<= mem_array (conv_integer1(A)) (15 downto 8) after tDBE;
                  end if;

                  if (Bc_b = '0') then
		               DQ (23 downto 16)<= mem_array (conv_integer1(A)) (23 downto 16) after tDBE;
                  end if; 
               
                  if (Bd_b = '0') then
   		               DQ (31 downto 24)<= mem_array (conv_integer1(A)) (31 downto 24) after tDBE;
                  end if;
            
           end if;
           
       end if;
       -- END of Bb/Ba controlled READ
       
       if (WE_b'last_event = read_enable'last_event) then

                  if (Ba_b = '0') then
		               DQ (7 downto 0)<= mem_array (conv_integer1(A)) (7 downto 0) after tACE;
                  end if; 
               
                  if (Bb_b = '0') then
   		               DQ (15 downto 8)<= mem_array (conv_integer1(A)) (15 downto 8) after tACE;
                  end if;

                  if (Bc_b = '0') then
		               DQ (23 downto 16)<= mem_array (conv_integer1(A)) (23 downto 16) after tACE;
                  end if; 
               
                  if (Bd_b = '0') then
   		               DQ (31 downto 24)<= mem_array (conv_integer1(A)) (31 downto 24) after tACE;
                  end if;

       end if;

     end if;
     --- END OF CE/OE/Ba/Bb/Bc/Bd controlled READ
   
    --- If either Ba or Bb or Bc or Bd toggle during read mode
    if (Ba_b'event and Ba_b = '0' and read_enable = '1' and not(read_enable'event)) then
	   DQ (7 downto 0) <= mem_array (conv_integer1(A)) (7 downto 0) after tDBE;
    end if;

    if (Bb_b'event and Bb_b = '0' and read_enable = '1' and not(read_enable'event)) then
	   DQ (15 downto 8) <= mem_array (conv_integer1(A)) (15 downto 8) after tDBE;
    end if;

    if (Bc_b'event and Bc_b = '0' and read_enable = '1' and not(read_enable'event)) then
	   DQ (23 downto 16) <= mem_array (conv_integer1(A)) (23 downto 16) after tDBE;
    end if;

    if (Bd_b'event and Bd_b = '0' and read_enable = '1' and not(read_enable'event)) then
	   DQ (31 downto 24) <= mem_array (conv_integer1(A)) (31 downto 24) after tDBE;
    end if;

    --- tri-state bus depending on Bb/Ba 
    if (Ba_b'event and Ba_b = '1') then
        DQ (7 downto 0) <= (others=>'Z') after tHZBE;
    end if;

    if (Bb_b'event and Bb_b = '1') then
        DQ (15 downto 8) <=(others=>'Z') after tHZBE;
    end if;

    if (Bc_b'event and Bc_b = '1') then
        DQ (23 downto 16) <= (others=>'Z') after tHZBE;
    end if;

    if (Bd_b'event and Bd_b = '1') then
        DQ (31 downto 24) <=(others=>'Z') after tHZBE;
    end if;

    wait on write_enable, A, read_enable, DQ, Ba_b, Bb_b, Bc_b, Bd_b, data_skew, address_skew;
    
end process;    
end behave_arch;
