`timescale 1 ns / 1 ps

module aubridge_gl_tb
(
);

  initial begin
    #10000000;
    $stop;
  end

  localparam SYSCLK_FREQ = 48_000_000;

  logic areset;
  logic clk;

  clockgen_tb#(
    .FREQ        (SYSCLK_FREQ),
    .RESET_DELAY (40.0),
    .JITTER      (0.000)
  )
    sysclk_gen
    (
      .areset (areset),
      .clk    (clk)
    );

  logic [ 3: 0]         a429_ena_tx2t;
  logic [ 3: 0][ 1: 0]  a429_ssel;
  always_comb
    a429_ssel = { 2'b00, 2'b01, 2'b10, 2'b11 };

  a429_serializer_divider#(
    .N       (4),
    .CLKFREQ (SYSCLK_FREQ)
  )
    a429_divider_enas
    (
      .areset     (areset),
      .clk        (clk),
      
      .ena_1ms    (),

      .ena_mes1t  (),
      .ena_mes4t  (),

      .ssel       (a429_ssel),
      .ena_tx2t   (a429_ena_tx2t)
    );

  logic         ser_ena;
  logic [31: 0] ser_d;
  logic         ser_busy;
  logic         ser_load;
  logic         ser_tx_a;
  logic         ser_tx_b;
    
  always_comb begin
    ser_ena   = a429_ena_tx2t[3];
    ser_load  = !ser_busy;
    ser_d     = 32'h12345678;
  end

  a429_serializer
    serializer
    (
      .areset (areset),
      .clk    (clk),

      .ena    (ser_ena),

      .d      (ser_d),

      .load   (ser_load),
      .busy   (ser_busy),

      .tx_a   (ser_tx_a),
      .tx_b   (ser_tx_b)
    );

//
  
  wire          clk48000kHz = clk;

  wire [ 7: 0]  AR429_RX_P;
  wire [ 7: 0]  AR429_RX_N;
  
  assign AR429_RX_P[7:0] = { 8 { ser_tx_a } };
  assign AR429_RX_N[7:0] = { 8 { ser_tx_b } };

//  assign AR429_RX_P[7:4]    = '0;
//  assign AR429_RX_N[7:4]    = '0;
      
  wire          AR429_RX_TEST74_P;
  wire          AR429_RX_TEST74_N;
  
  wire          AR429_RX_TEST30_P;
  wire          AR429_RX_TEST30_N;

  // ARINC 429 transmitters pins (HI-8596PS)
  wire [ 3: 0]  AR429_TX_P;
  wire [ 3: 0]  AR429_TX_N;

  // UART pins
  wire          UART_RX = '0;
  wire          UART_TX;
  
  // Asynchronous static RAM
  wire [ 2: 0]  ASRAM_CE_N;
  wire [31: 0]  ASRAM_D;
  wire [18: 0]  ASRAM_A;
  wire [ 3: 0]  ASRAM_BE_N;
  wire          ASRAM_WE_N;
  wire          ASRAM_OE_N;

  CY7C1062_tb1
    ram
    (
      .ASRAM_CE_N (ASRAM_CE_N),
      .ASRAM_D    (ASRAM_D),
      .ASRAM_A    (ASRAM_A),
      .ASRAM_BE_N (ASRAM_BE_N),
      .ASRAM_WE_N (ASRAM_WE_N),
      .ASRAM_OE_N (ASRAM_OE_N)
    );

  aubridge
    aubridge_top
    (
      .*
    );

endmodule
