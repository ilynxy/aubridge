module CY7C1062_tb1
(
  input   wire [ 2: 0]  ASRAM_CE_N,
  inout   wire [31: 0]  ASRAM_D,
  input   wire [18: 0]  ASRAM_A,
  input   wire [ 3: 0]  ASRAM_BE_N,
  input   wire          ASRAM_WE_N,
  input   wire          ASRAM_OE_N
);

  async_512kx32
    ram
    (
      .CE1_b (ASRAM_CE_N[0]),   
      .CE2_b (ASRAM_CE_N[1]), 
      .CE3_b (ASRAM_CE_N[2]),
      .WE_b  (ASRAM_WE_N),
      .OE_b  (ASRAM_OE_N),
      .Ba_b	 (ASRAM_BE_N[0]),
      .Bb_b  (ASRAM_BE_N[1]),
      .Bc_b	 (ASRAM_BE_N[2]),
      .Bd_b  (ASRAM_BE_N[3]),
      .A 		 (ASRAM_A),
      .DQ		 (ASRAM_D)
    ); 

endmodule
