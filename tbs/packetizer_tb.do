transcript on
if {[file exists packetizer_tb]} {
  vdel -lib packetizer_tb -all
}
vlib packetizer_tb
vmap work packetizer_tb

vlog -sv -work packetizer_tb  packetizer_tb.sv
vlog -sv -work packetizer_tb  clockgen_tb.sv
vlog -sv -work packetizer_tb  ../frame_packetizer.sv
vlog -sv -work packetizer_tb  ../generic_crc.sv
vlog -sv -work packetizer_tb  ../UART_serializer.sv
vlog -sv -work packetizer_tb  ../UART_bitrate_divider.sv

vsim -t 1ps -L packetizer_tb -voptargs="+acc"  packetizer_tb

add wave -noupdate /packetizer_tb/areset
add wave -noupdate /packetizer_tb/clk

add wave -noupdate /packetizer_tb/ser_ena
add wave -noupdate -radix hexadecimal /packetizer_tb/ser_d
add wave -noupdate /packetizer_tb/ser_load
add wave -noupdate /packetizer_tb/ser_busy
add wave -noupdate /packetizer_tb/ser_tx

view structure
view signals

run -all

wave zoom full
