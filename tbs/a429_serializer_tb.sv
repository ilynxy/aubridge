`timescale 1 ns / 1 ps

module a429_serializer_tb
(
);

  initial begin
    #100000000;
    $stop;
  end

  localparam SYSCLK_FREQ = 48_000_000;
  localparam UART_BITRATE =  2_000_000;

  logic areset;
  logic clk;

  clockgen_tb#(
    .FREQ        (48.0e6),
    .RESET_DELAY (40.0),
    .JITTER      (0.000)
  )
    sysclk_gen
    (
      .areset (areset),
      .clk    (clk)
    );

  logic                 a429_ena_mes1t;
  logic                 a429_ena_mes4t;
  logic [ 3: 0]         a429_ena_tx2t;
  logic [ 3: 0][ 1: 0]  a429_ssel;
  always_comb
    a429_ssel = { 2'b00, 2'b01, 2'b10, 2'b11 };

  a429_serializer_divider#(
    .N       (4),
    .CLKFREQ (SYSCLK_FREQ)
  )
    a429_divider_enas
    (
      .areset     (areset),
      .clk        (clk),

      .ena_mes1t  (a429_ena_mes1t),
      .ena_mes4t  (a429_ena_mes4t),

      .ssel       (a429_ssel),
      .ena_tx2t   (a429_ena_tx2t)
    );

  logic         ser_ena;
  logic [31: 0] ser_d;
  logic         ser_busy;
  logic         ser_load;
  logic         ser_tx_a;
  logic         ser_tx_b;
  
  
  always_comb begin
    ser_ena   = a429_ena_tx2t[0];
    ser_load  = !ser_busy;
    ser_d     = 32'h12345678;
  end

  a429_serializer
    serializer
    (
      .areset (areset),
      .clk    (clk),

      .ena    (ser_ena),

      .d      (ser_d),

      .load   (ser_load),
      .busy   (ser_busy),

      .tx_a   (ser_tx_a),
      .tx_b   (ser_tx_b)
    );

  int a_p;
  int a_n;
  always_comb begin
    if (ser_tx_a == 0 && ser_tx_b == 0)
      a_p = 0;
    else
      a_p = ser_tx_a ? 1 : -1;
    a_n = -a_p;
  end
  
  logic [31: 0] deser_q;
  logic         deser_valid;
  
  a429_deserializer
    deserializer
    (
      .areset     (areset),
      .clk        (clk),

      .ena_mes1t  (a429_ena_mes1t),
      .ena_mes4t  (a429_ena_mes4t),

      .rx_a       (ser_tx_a),
      .rx_b       (ser_tx_b),

      .q          (deser_q),
      .valid      (deser_valid),
      .flush      (1'b0)
    );

endmodule
