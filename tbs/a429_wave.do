onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /a429_serializer_tb/SYSCLK_FREQ
add wave -noupdate /a429_serializer_tb/UART_BITRATE
add wave -noupdate /a429_serializer_tb/areset
add wave -noupdate /a429_serializer_tb/clk
add wave -noupdate /a429_serializer_tb/ser_ena
add wave -noupdate /a429_serializer_tb/ser_d
add wave -noupdate /a429_serializer_tb/ser_busy
add wave -noupdate /a429_serializer_tb/ser_load
add wave -noupdate /a429_serializer_tb/ser_tx_a
add wave -noupdate /a429_serializer_tb/ser_tx_b
add wave -noupdate -radix unsigned /a429_serializer_tb/serializer/bc
add wave -noupdate /a429_serializer_tb/serializer/r_sh
add wave -noupdate -format Analog-Step -height 40 -max 1.0 -min -1.0 /a429_serializer_tb/a_p
add wave -noupdate -format Analog-Step -height 40 -max 1.0 -min -1.0 /a429_serializer_tb/a_n
add wave -noupdate /a429_serializer_tb/a429_divider_enas/r_ena_tx2t
add wave -noupdate /a429_serializer_tb/a429_divider_enas/r_ena_mes1t
add wave -noupdate /a429_serializer_tb/a429_divider_enas/r_ena_mes4t
add wave -noupdate /a429_serializer_tb/a429_divider_enas/mcnt
add wave -noupdate /a429_serializer_tb/a429_divider_enas/prediv_wrap
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {6312702 ps} 0} {{Cursor 4} {16313022 ps} 0} {{Cursor 5} {86159964 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 273
configure wave -valuecolwidth 119
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 41666
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {26250 ns}
