`timescale 1 ns / 1 ps

module uart_tx_tb
(
);

  initial begin
    #140000;
    $stop;
  end

  localparam SYSCLK_FREQ = 48_000_000;
  localparam UART_BITRATE =  2_000_000;

  logic areset;
  logic clk;

  clockgen_tb#(
    .FREQ        (48.0e6),
    .RESET_DELAY (40.0),
    .JITTER      (0.000)
  )
    sysclk_gen
    (
      .areset (areset),
      .clk    (clk)
    );

  logic           ser_ena;
  logic [ 7: 0]   ser_d;
  logic           ser_load = '0;
  logic           ser_busy;
  logic           ser_tx;

  UART_bitrate_divider#(
    .CLKFREQ  (SYSCLK_FREQ),
    .BITRATE  (UART_BITRATE)
  )
    serializer_bitrate_ena
    (
      .areset   (areset),
      .clk      (clk),

      .tx_ena   (ser_ena)
    );
    
  UART_serializer
    serializer
    (
      .areset   (areset),
      .clk      (clk),
  
      .ena      (ser_ena),
  
      .d        (ser_d),
      .load     (ser_load),
      .busy     (ser_busy),

      .stx      (ser_tx)
    );
/*
  dbg_serializer0
    dbg_ser
    (
      .*
    );
*/

  initial begin
    #200 
    send_array('{ 8'h00, 8'h00, 8'h00, 8'h00, 8'h00, 8'h00 });
    
    write_memory_block(24'h010000, '{ 32'h01234567, 32'h01234567, 32'h01234567, 32'h01234567, 32'h01234567, 32'h01234567, 32'h01234567, 32'h01234567 });
  end


  logic [ 7: 0] deser_q;
  logic         deser_valid;
  
  UART_deserializer#(
    .CLKFREQ  (SYSCLK_FREQ),
    .BITRATE  (UART_BITRATE)
  )
    deserializer
    (
      .areset (areset),
      .clk    (clk),

      .rx     (ser_tx),

      .q      (deser_q),
      .valid  (deser_valid)
    );

  typedef logic [ 7: 0] uint8_t;
  typedef logic [31: 0] uint32_t;
  
  task automatic send_byte
  (
    input   logic [ 7: 0] x
  );
    ser_d    = x;

    forever @(posedge ser_ena) begin
      ser_load = !ser_busy;

      if (ser_load)
        break;
    end

    @(posedge clk)    

    ser_load = '0;
    ser_d    = 'x;

  endtask : send_byte

  task automatic send_array(input uint8_t x[]);
    
    foreach (x[i])
      send_byte(x[i]);

  endtask : send_array

  virtual class crc #(
    parameter CRC_WIDTH     = 8,
    parameter CRC_POLYNOME  = 8'h07,
    parameter DATA_WIDTH    = 8,

    parameter DATA_REFLECT  = 0,
    parameter CRC_REFLECT   = 0
  );
    typedef logic [DATA_WIDTH-1:0] data_t;
    typedef logic [CRC_WIDTH-1:0]  crc_t;

    static function crc_t  reflect_crc(input crc_t c);
      for (int i = 0; i < CRC_WIDTH; i ++)
        reflect_crc[i] = c[CRC_WIDTH-1 - i];
    endfunction : reflect_crc

    static function crc_t crc_next(input data_t d, input crc_t c);
      crc_t v;
      v = c;    
      for (int i = 0; i < DATA_WIDTH; i ++) begin
        logic b;

        if (DATA_REFLECT != 0) begin
          b = d[0];
          d = d >> 1;
        end
        else begin
          b = d[DATA_WIDTH-1];
          d = d << 1;
        end

        if (CRC_REFLECT != 0) begin
          b = b ^ v[0];
          v = (v >> 1) ^ (reflect_crc(CRC_POLYNOME) & { CRC_WIDTH { b} });
        end
        else begin
          b = b ^ v[CRC_WIDTH-1];
          v = (v << 1) ^ (CRC_POLYNOME & { CRC_WIDTH { b } });
        end
      end

      crc_next = v;
    endfunction : crc_next

  endclass

  task automatic write_memory_block(input logic [23: 0] offset, input uint32_t data[]);
    
    typedef crc#() cm_t;

    cm_t::crc_t   c;
    cm_t::data_t  d;
    
    c = '1;
    d =         8'h55; send_byte(d); c = cm_t::crc_next(d, c);
    d = offset[ 7: 0]; send_byte(d); c = cm_t::crc_next(d, c);
    d = offset[15: 8]; send_byte(d); c = cm_t::crc_next(d, c);
    d = offset[23:16]; send_byte(d); c = cm_t::crc_next(d, c);
    d =   $size(data); send_byte(d); c = cm_t::crc_next(d, c);
    d =             c; send_byte(d); c = cm_t::crc_next(d, c);

    foreach(data[i]) begin
      c = '1;
      d = data[i][ 7: 0]; send_byte(d); c = cm_t::crc_next(d, c);
      d = data[i][15: 8]; send_byte(d); c = cm_t::crc_next(d, c);
      d = data[i][23:16]; send_byte(d); c = cm_t::crc_next(d, c);
      d = data[i][31:24]; send_byte(d); c = cm_t::crc_next(d, c);
      d =              c; send_byte(d); c = cm_t::crc_next(d, c);
    end

  endtask : write_memory_block

endmodule
