`timescale 1 ns / 1 ps

module packetizer_tb
(
);

  initial begin
    #140000;
    $stop;
  end

  localparam SYSCLK_FREQ = 48_000_000;
  localparam UART_BITRATE =  2_000_000;

  logic areset;
  logic clk;

  clockgen_tb#(
    .FREQ        (48.0e6),
    .RESET_DELAY (40.0),
    .JITTER      (0.000)
  )
    sysclk_gen
    (
      .areset (areset),
      .clk    (clk)
    );

  logic           commit;
  
  initial begin
    commit = '0;

    #10000
      @(posedge clk)
        commit = '1;
      @(posedge clk)
        commit = '0;
  end
    
    
  logic           ser_ena;
  logic [ 7: 0]   ser_d;
  logic           ser_load;
  logic           ser_busy;
  logic           ser_tx;
  
  logic         fa_rs_ena;
  logic         fa_rs_busy;
  logic [31: 0] fa_rs_q;
  logic [ 7: 0] fa_rs_words;

  always_comb begin
    fa_rs_busy  = '0;
    fa_rs_q     = 32'h11223344;
    fa_rs_words = 4;
  end

  frame_packetizer
    fgen
    (
      .areset   (areset),
      .clk      (clk),

      .commit   (commit),

      .rs_words ( (fa_rs_words >> 1) ),

      .rs_ena   (fa_rs_ena),
      .rs_busy  (fa_rs_busy),
      .rs_q     (fa_rs_q),

      .ser_ena  (ser_ena),
      .ser_d    (ser_d),
      .ser_load (ser_load),
      .ser_busy (ser_busy)
    );
  
  UART_bitrate_divider#(
    .CLKFREQ  (SYSCLK_FREQ),
    .BITRATE  (UART_BITRATE)
  )
    serializer_bitrate_ena
    (
      .areset   (areset),
      .clk      (clk),

      .tx_ena   (ser_ena)
    );
    
  UART_serializer
    serializer
    (
      .areset   (areset),
      .clk      (clk),
  
      .ena      (ser_ena),
  
      .d        (ser_d),
      .load     (ser_load),
      .busy     (ser_busy),

      .stx      (ser_tx)
    );

endmodule
