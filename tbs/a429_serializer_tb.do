# ARINC 429 serializer testbench

set library_name {a429_serializer_tb}

set file_list {
  {a429_serializer_tb.sv}
  {clockgen_tb.sv}
  {../a429_serializer.sv}
  {../a429_serializer_divider.sv}
  {../a429_deserializer.sv}
  {../a429_HI8444PS_adapter.sv}
}

set top_level {a429_serializer_tb}

proc tbcompile {} {
  uplevel #0 source compile_tb.tcl
}

proc tbrun {} {
  restart -force
  run -all
}

tbcompile

eval vsim -voptargs="+acc" $top_level

tbrun
