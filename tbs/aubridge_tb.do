# ARINC 429 serializer testbench

set library_name {aubridge_tb}

set file_list {
  {aubridge_tb.sv}
  {clockgen_tb.sv}
  {CY7C1062_tb.sv}
  {CY7C1062_tb1.sv}
  {package_timing.vhd}
  {package_utility.vhd}
  {async_512Kx32.vhd}
  {../aubridge.sv}
  {../ram_manager.sv}
  {../scc_fifo.sv}
  {../arinc429_receiver.sv}
  {../frame_packetizer.sv}
  {../UART_bitrate_divider.sv}
  {../UART_serializer.sv}
  {../asram_CY7C1062_adapter.sv}
  {../ram_arbitrator.sv}
  {../a429_queuer.sv}
  {../generic_crc.sv}
  {../a429_serializer.sv}
  {../a429_serializer_divider.sv}
  {../a429_deserializer.sv}
  {../a429_HI8444PS_adapter.sv}
  {../a429_player.sv}
  {../a429_player_fsm.sv}
  {../UART_deserializer.sv}
  {../uart_command_parser.sv}
  {../onewire_DS18B20.sv}
  {../crossclock/ccpipe.sv}
  {../ar429_rx_activity_led.sv}
}

set top_level {aubridge_tb}

proc tbcompile {} {
  uplevel #0 source compile_tb.tcl
}

proc tbupdate {} {
  set last_compile_time 0
  tbcompile
}

proc tbrun {} {
  restart -force
  run -all
}

tbupdate

eval vsim -L altera_mf_ver -voptargs="+acc" $top_level

# tbrun
