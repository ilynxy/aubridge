`timescale 1 ns / 1 ps
`default_nettype none

module clockgen_tb
#(
  parameter FREQ        = 65.536e6,
  parameter RESET_DELAY = 40.0,
  parameter PERIOD      = 1.0e9 / FREQ,
  parameter JITTER      = 0.0,
  parameter SEED        = 0
)
(
  output logic areset,
  output logic clk
);

  initial begin
    areset = '1;
    clk    = '1;
    
    #(RESET_DELAY)
      @(posedge clk)
        areset = 1'b0;
  end

  integer s = SEED;
  real    j;
  
  always begin
    j  = ($dist_uniform(s,-10000, 10000) / 10000.0) * PERIOD/2 * JITTER;
    #(PERIOD/2 + j) clk   = ~clk;
  end

endmodule
